#include "ForceTask.hpp"


LoopTask* create_task(ISEAHardware* hw)
{
    ForceTask* ft = new ForceTask(hw);
    ft->logEnabled = true;
	ft->maxDuration = 60;

	return ft;
}


//faulhaber
#define KP 10.0//80.0
#define KD 0.0
#define KI 0.0

ForceTask::ForceTask(ISEAHardware* hw): LoopTask(hw)
{
    pd = new MotorPID(KI,KP,KD,hw);

    double lambda1, lambda2, f;
	f = 1;
	lambda2 = powf(2*M_PI*f,2);
	lambda1 = 2.0*sqrt(lambda2);
    mrac = new MAdaptiveForceControl(lambda1,lambda2,hw);
    mrac->logEnabled = true;
    mrac->referenceModelEnabled = true;
    mrac->adaptationEnabled = false;
//    mrac->setAdaptationSpeed(10);
//    mrac->L = 2*M_PI*20.0;



    filename = "force";

    ctr = mrac;
}

void ForceTask::logOpen()
{
	filenameStream << filename  << "-ref_"<< ctr->ref << ".csv";
	string s(filenameStream.str());
	logfile.open(s.c_str());
}

int ForceTask::_loop()
{
    ctr->ref = 0;
    ctr->dref = 0;
    ctr->ddref = 0;
	ctr->ref = 0.5*sin(1.5 * M_PI * 2 * time);
	ctr->dref = 1.5 * M_PI * 2 * 0.5*cos(1.5 * M_PI * 2 * time);

    current = ctr->process(ctr->tau,ctr->dtau,dt);
//	current = ctr->process(ctr->torqueObserver,ctr->diffTorqueObserver,dt);
    current += ctr->frictionCurrentCompensation(hw->getThetaM());
    hw->setCurrent(current);
    return 1;
}


void ForceTask::Log()
{
    logfile << time << " ";
    logfile << hw->getCurrent() << " ";
    logfile << current << " ";
    logfile << hw->getThetaM() << " ";
    logfile << hw->getDThetaM() << " ";
    logfile << hw->getTorque() << " ";
    logfile << hw->getDiffTorque() << " ";
    logfile << ctr->ref << " ";
    logfile << ctr->dref << " ";
    logfile << ctr->torqueObserver << " ";
    logfile << ctr->diffTorqueObserver << " ";
    logfile << endl;
}
