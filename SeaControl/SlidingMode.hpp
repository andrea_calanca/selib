#ifndef SLIDINGMODE_H
#define SLIDINGMODE_H

#include <sstream>
#include <string>


#include "defines.h"
#include "ControlBase.hpp"
#include "DigitalFilter.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

class SlidingModeControlBase: public SEAControl{
public:
	~SlidingModeControlBase(){delete res; delete ddFilter;};
	SlidingModeControlBase(double lambda, ISEAHardware* hw);
    double ___process(double dt);
    virtual double slidingSurface(double err,double derr) = 0;
    virtual double controlLaw() = 0;
	double lambda1;
	double ni, phi;

protected:
	void Log();
    double err, derr, s;
	double us;
	DigitalFilter* res;
	DigitalFilter* ddFilter;

};

/**
This class implements the sliding-mode control law proposed in

A. Calanca, L. Capisani, and P. Fiorini, Robust Force Control of Series Elastic Actuators, Actuators, Spec. Issue Soft Actuators, vol. 3, no. 3, pp. 182 204, 2014.

to control a SEA force/torque
*/
class SlidingModeForceControl: public SlidingModeControlBase{
public:
	SlidingModeForceControl(double lambda, ISEAHardware* hw);
    virtual double slidingSurface(double err,double derr);
    virtual double controlLaw();
protected:
	double kp, kd;

};

/**
This class implements the integral sliding-mode control law proposed in

A. Calanca, L. Capisani, and P. Fiorini, Robust Force Control of Series Elastic Actuators, Actuators, Spec. Issue Soft Actuators, vol. 3, no. 3, pp. 182 204, 2014.

to control a SEA force/torque
*/
class IntegralSlidingModeForceControl: public SlidingModeControlBase{
public:
	IntegralSlidingModeForceControl(double l1, double l2, ISEAHardware* hw);
    double slidingSurface(double err,double derr);
    double controlLaw();
	double lambda2;
	double damp,freq;

protected:
	double ierr;
	double kp, kd;
	DigitalFilter* res;
};

/**
This class implements the super twisting sliding-mode control law used in

A. Calanca, L. Capisani, and P. Fiorini, Robust Force Control of Series Elastic Actuators, Actuators, Spec. Issue Soft Actuators, vol. 3, no. 3, pp. 182 204, 2014.

to control a SEA force/torque
*/
class SuperTwistingControl: public SEAControl{
public:
	SuperTwistingControl(double lambda, ISEAHardware* hw);
    double ___process(double dt);

	//parameters
    double ki,ni,rho, a;

protected:

	//errors
    double err,derr,ierr,s,lambda;
	//auxiliary variable
	double ua, dua, utw;

	double prev_dtheta_e, ddtheta_e, ddtheta_ef;
	DigitalFilter* ddFilter;

	void Log();

};

#endif // SLIDINGMODE_H
