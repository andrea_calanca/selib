#include "EnergyControl.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

EnergyControl::EnergyControl(ISEAHardware* hw) : SEAControl(hw)
{
    filter = DigitalFilter::getLowPassFilterHz(20);
	this->filename = "PEA-energy-";
	mu = 1 + JL/JM;
	w = sqrt(K_SPRING*(1/JM + 1/JL));
//	energy_ref = 0.1;
//	k_e = 0.1;

}

void EnergyControl::logOpen()
{
	filenameStream << filename << "-en_" << energy_ref << "-k_e_" << k_e << ".csv";
	string s(filenameStream.str());
	logfile.open(s.c_str());
}

double EnergyControl::___process(double dt)
{

	energy = 0.5 * (JM*pow(dtheta_m,2) + JL*pow(dtheta_e,2) + K_SPRING*pow(theta_m-theta_e,2));
	e = energy_ref - energy;
    kv = k_e*e;
	out = kv*(dtheta_m);
//	out = kv*sin(w*time);

	//friction compensation
	fcomp = frictionTorqueCompensationColoumb(dtheta_m);
//	fcomp = frictionTorqueCompensation(dtheta_m);

	fcomp += sign(dtheta_e)*0.012 * KT;
//	fcomp += dtheta_e*0.0002 * KT;

	cout << energy;
	return out + fcomp;
}

void EnergyControl::Log()
{

    logfile << time << " ";
    logfile << hw->getThetaM() << " ";
    logfile << hw->getDThetaM() << " ";
    logfile << hw->getThetaE() << " ";
    logfile << hw->getDThetaE() << " ";
    logfile << energy << " ";
    logfile << energy_ref << " ";
	logfile << hw->getTauM() << " ";
    logfile << out << " ";
    logfile << kv << " ";
    logfile << endl;
}
