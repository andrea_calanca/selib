#ifndef ISEAHARDWARE_H
#define ISEAHARDWARE_H

#include <iostream>

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

using namespace std;

/**
This class describes the interface with the HW electronics. You must extend this class and fill all the abstract methods to make the library working with your HW. See the child class BeckhoffSEAHardware as example.
*/
class IMotorHardware
{
	public:

        virtual void refresh(double dt) = 0; ///< this method is called in the main LoopTask cycle and is supposed to update the class fields with hardware data

     	virtual void setCurrent(double value) = 0; ///< sets the current to the motor
        virtual void setTauM(double value) = 0; ///< sets the torque to the motor (will be then converted in a current)

        virtual double getCurrent() = 0; ///< reads the active motor current
        virtual double getTauM() = 0; ///< reads the motor torque (relative to the the active motor current)

        virtual double getThetaM() = 0; ///< reads the motor angle or position
        virtual double getDThetaM() = 0; ///< reads the motor velocity
        virtual double getDiffThetaM() = 0; ///< computes the motor velocity based on position numerical differences
        virtual double getDDiffThetaM() = 0; ///< computes the motor accelleration based on numerical velocity differences

        virtual double getTorque() = 0; ///< reads the force or torque sensor if present, otherwise it returns k*(theta_m - theta_e)
        virtual double getDiffTorque() = 0; ///< compute the force or torque derivative

		virtual void resetM() = 0; ///< resets the motor encoder

		virtual bool isStandStill(int time) = 0; ///< returns if the SEA (motor and load) is stand still
		virtual bool isSaturated() = 0; ///< returns if the motor reached the saturation within the previous loop

        virtual bool getIndexM(){ cout<< "The encoder doesn't have the index signal"; return false;} ///< returns the index signal of the motor encoder
};


/**
This class describes the interface with the HW electronics. You must extend this class and fill all the abstract methods to make the library working with your HW. See the child class BeckhoffSEAHardware as example.
*/
class ISEAHardware: IMotorHardware
{
    public:

        virtual void refresh(double dt) = 0; ///< this method is called in the main LoopTask cycle and is supposed to update the class fields with hardware data

     	virtual void setCurrent(double value) = 0; ///< sets the current to the motor
        virtual void setTauM(double value) = 0; ///< sets the torque to the motor (will be then converted in a current)

        virtual double getCurrent() = 0; ///< reads the active motor current
        virtual double getTauM() = 0; ///< reads the motor torque (relative to the the active motor current)

        virtual double getThetaM() = 0; ///< reads the motor angle or position
        virtual double getThetaE() = 0; ///< reads the load/environment angle or position
        virtual double getDThetaM() = 0; ///< reads the motor velocity
        virtual double getDThetaE() = 0; ///< reads the load/environment velocity
        virtual double getDiffThetaM() = 0; ///< computes the motor velocity based on position numerical differences
        virtual double getDiffThetaE() = 0; ///< computes the load/environment velocity based on position numerical differences
        virtual double getDDiffThetaM() = 0; ///< computes the motor accelleration based on numerical velocity differences
        virtual double getDDiffThetaE() = 0; ///< computes the load/environment accelleration based on velocity numerical differences

        virtual double getTorque() = 0; ///< reads the force or torque sensor if present, otherwise it returns k*(theta_m - theta_e)
        virtual double getDiffTorque() = 0; ///< compute the force or torque derivative

		virtual void resetM() = 0; ///< resets the motor encoder
		virtual void resetE() = 0; ///< resets the load encoder

		virtual bool isStandStill(int time) = 0; ///< returns if the SEA (motor and load) has been stand still in the last time interval
		virtual bool isSaturated() = 0; ///< returns if the motor reached the saturation within the previous loop

        virtual bool getIndexM(){ cout<< "The encoder doesn't have the index signal"; return false;} ///< returns the index signal of the motor encoder
        virtual bool getIndexE(){ cout<< "The encoder doesn't have the index signal"; return false;} ///< returns the index signal of the load encoder

};

#endif // ISEAHARDWARE_H
