#include "ControlBase.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/


MotorControlBase::MotorControlBase(ISEAHardware *hw)
{
	this->hw = hw;
    thetaMFilter = NULL;
    dthetaMFilter = DigitalFilter::getLowPassFilterHz(20);
    ddthetaMFilter = DigitalFilter::getLowPassFilterHz(20);

    torqueFilter = NULL;//DigitalFilter::get50HzFilter();
    dtorqueFilter = DigitalFilter::getLowPassFilterHz(20);
    ddtorqueFilter = DigitalFilter::getLowPassDifferentiatorHz(20);

    torqueObserverFilter = DigitalFilter::getLowPassFilterHz(10);
    g = 2*31.42;
}

double MotorControlBase::_process(double theta, double dtheta, double dt)
{
	// get and filter motor data

	// motor position
	theta_m = hw->getThetaM();
	dtheta_m = hw->getDThetaM();
	ddtheta_m = hw->getDDiffThetaM();
    if(thetaMFilter != NULL) theta_m = thetaMFilter->process(hw->getThetaM());
    if(dthetaMFilter != NULL) dtheta_m = dthetaMFilter->process(hw->getDiffThetaM());
    if(ddthetaMFilter != NULL) ddtheta_m = ddthetaMFilter->process(hw->getDDiffThetaM());

	// motor torque (measured from sensor)
	tau = hw->getTorque();
	dtau = hw->getDiffTorque();
	if(torqueFilter != NULL) tau = torqueFilter->process( hw->getTorque() );
	if(dtorqueFilter != NULL) dtau = dtorqueFilter->process( hw->getDiffTorque() );
	ddtau = ddtorqueFilter->process( hw->getDiffTorque() );

	// torque observer (virtual torque sensor)
	torqueObserver = torqueObserverFilter->process(KT*hw->getCurrent() + g*JM*dtheta_m - frictionTorqueCompensation(dtheta_m)) - g*JM*dtheta_m;
	diffTorqueObserver = (torqueObserver - prev_torqueObserver) / dt;

	prev_torqueObserver = torqueObserver;

	return __process(theta, dtheta, dt);
}

SEAControl::SEAControl(ISEAHardware *hw): MotorControlBase(hw)
{
	this->hw = hw;
	this->torqueFromDisplacement = false;
	thetaEFilter = NULL;
    dthetaEFilter = DigitalFilter::getLowPassFilterHz(20);
	ddthetaEFilter = DigitalFilter::getLowPassFilterHz(20);
}

double SEAControl::__process(double theta, double dtheta, double dt)
{
	theta_e = hw->getThetaE();
	dtheta_e = hw->getDThetaE();
	ddtheta_e = hw->getDDiffThetaE();

    if(thetaEFilter != NULL) theta_e = thetaEFilter->process(hw->getThetaE());
    if(dthetaEFilter != NULL) dtheta_e = dthetaEFilter->process(hw->getDiffThetaE());
    if(ddthetaEFilter != NULL) ddtheta_e = ddthetaEFilter->process(hw->getDDiffThetaE());

    // we may want to overwrite the torque measured from sensor with the torque measured from dispacement
    if(torqueFromDisplacement)
    {
    tau = K_SPRING * (theta_m - theta_e);
	dtau = K_SPRING * ( dtheta_m - dtheta_e );
	ddtau = K_SPRING*(ddtheta_m-ddtheta_e);
    }

	return ___process(dt);
}

SEAControl::~SEAControl()
{
	delete thetaEFilter;
	delete dthetaEFilter;
	delete ddthetaEFilter;

}

MotorControlBase::~MotorControlBase()
{
	delete thetaMFilter;
	delete dthetaMFilter;
	delete ddthetaMFilter;
	delete torqueFilter;
	delete dtorqueFilter;
	delete torqueObserverFilter;
}

double ControlBase::saturation(double in, double threshold)
{
    double sat;
    sat = max(in, -threshold);
    return min(sat, threshold);
}

double ControlBase::saturation(double in, double lowThreshold, double upThreshold)
{
    double  sat;
	sat = max(in, lowThreshold);
    return min( sat, upThreshold);
}

void ControlBase::refSaturation(double lowThreshold, double upThreshold)
{
	if(ref < lowThreshold)
    {
    	ref = lowThreshold;
    	dref = 0;
    	ddref = 0;
    }
	else if(ref > upThreshold)
    {
    	ref = upThreshold;
    	dref = 0;
    	ddref = 0;
    }
}


PID::PID(double ki, double kp, double kd)
{
	KP = kp;
	KD = kd;
	KI = ki;
    integralSAT = 10;
	outFilter = NULL;//DigitalFilter::get50HzFilter();
	velFilter = NULL;
}

double PID::_process(double pos, double vel, double deltaTime)
{
    err = ref - pos;
	derr = (dref - vel);
	if(velFilter != NULL) derr = velFilter->process(derr);
	ierr += err * deltaTime;
	ierr = saturation(ierr,integralSAT);
	out = KP*err + KD*derr + KI*ierr;
    if(outFilter != NULL) out = outFilter->process(out);
	return out;
}


MotorPID::MotorPID(double ki, double kp, double kd, ISEAHardware* hw): MotorControlBase(hw)
{
	this->hw = hw;
	pid = new PID(ki,kp,kd);
}

double MotorPID::__process(double y, double dy, double dt)
{
	pid->ref = this->ref;
	pid->dref = this->dref;
	pid->ddref = this->ddref;
	return pid->process(y,dy,dt);
}

