#ifndef MOTORCONTROL_H
#define MOTORCONTROL_H

#include"defines.h"
#include"AdaptiveControl.hpp"



class MAdaptiveForceControl: public MotorControlBase, public ModelReference2
{
public:
	MAdaptiveForceControl(double l1, double l2, ISEAHardware* hw);
	~MAdaptiveForceControl(){};

	PID* pid;

    double __process(double theta, double dtheta, double dt);

    virtual void adaptationUpdate(double dt);

	virtual void setAdaptationSpeed(double g){G = g;};

	bool adaptationEnabled;
	bool referenceModelEnabled;

	double L; ///< algorithm convergence rate. Usually the higher the better. In the MRAC case this parameter does not influence either control bandwidth nor adaptation speed. It influences the precision -> this is shown in “Human-Adaptive Control of Series Elastic Actuators,” Robotica, vol. 2, no. 08, pp. 1301–1316, 2014. Some unstability can be seen for very low setting

	double a_est; ///< estimate of the parameter a
	double ddtheta0_est; ///< estimate of the parameter ddtheta0

    double ni; ///< sliding-mode gain
    double phi; ///< sliding boundary width

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    bool gradientUpdate; ///< set the gradient (=true) or recursive least square (=false) update

protected:
	double G; ///< adaptation speed
    double us;

	void Log();

    double x_tilde, dx_tilde, nu, s;
    double da_est;
    double log1, log2, log3;



	double w0, w1, w2;
	double err, y;
	double lambda, g;
	double kp, kd, dkp, dkp_max;

	Vector2d anew_theta, theta, dtheta, w;
	Matrix2d P, dP, rho, Q;

	DigitalFilter *w0filt, *w1filt, *w2filt;
	DigitalFilter  *ddtheta0Filter;

};

//lass MAdaptiveForceControl: public MotorControlBase, public ModelReference2
//{
//public:
//	MAdaptiveForceControl(double l1, double l2, ISEAHardware* hw);
//	~MAdaptiveForceControl(){};
//
//    double __process(double theta, double dtheta, double dt);
//
//    virtual void adaptationUpdate(double dt);
//
//	virtual void setAdaptationSpeed(double g){G = g;};
//
//	bool adaptationEnabled;
//	bool referenceModelEnabled;
//
//	double L; ///< algorithm convergence rate. Usually the higher the better. In the MRAC case this parameter does not influence either control bandwidth nor adaptation speed. It influences the precision -> this is shown in “Human-Adaptive Control of Series Elastic Actuators,” Robotica, vol. 2, no. 08, pp. 1301–1316, 2014. Some unstability can be seen for very low setting
//
//	double a_est; ///< estimate of the parameter a
//	double ddtheta0_est; ///< estimate of the parameter ddtheta0
//
//    double ni; ///< sliding-mode gain
//    double phi; ///< sliding boundary width
//
//
//protected:
//	double G; ///< adaptation speed
//    double us;
//
//	void Log();
//
//    double x_tilde, dx_tilde, nu, s;
//    double da_est;
//    double log1, log2, log3;
//};

#endif // MOTORCONTROL_H
