#include <iostream>
#include <stdio.h>
#include "SEAHardware.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

BeckhoffSEAHardware::BeckhoffSEAHardware(): EthercatPlugin("SEA")
{
	srand (time(NULL));
	toRadians = 1.0/(double)ENCODER_STEPS*2*M_PI;
	prev_torque = 0;

	envelopeFilterM = DigitalFilter::getLowPassFilterHz(1);
	envelopeFilterE = DigitalFilter::getLowPassFilterHz(1);

}

double BeckhoffSEAHardware::curSaturation(double in)
{
    if(fabs(in) > CUR_SAT)
    {
		cout<<"SATURATION!!";
		saturation_flag = true;
		return CUR_SAT*sign(in);
    }
   else
   {
   	saturation_flag = false;
   	return in;
   }
}


void BeckhoffSEAHardware::refresh(double dt)
{
    //torque/current
    active_current = (double)analogSensorIn[0]*READ_CURRENT_CONVERSION; // current reference
    active_tau_m = active_current*KT; // torque reference

    torque = (double)analogSensorIn[1]*READ_TORQUE_CONVERSION - TORQUE_SENSOR_OFFSET;
    difftorque =  (torque - prev_torque) / dt;

    //encoder position in rad
    thetaM = -(double)(encodersPositionIn[MOTOR]-zeroMOTOR)*toRadians; //encoders
    thetaE = -(double)(encodersPositionIn[LOAD]-zeroLOAD)*toRadians; //encoders


    //encoder velocity in rad
    //1. diff velocity
    diffthetaM = (double)(-encodersPositionIn[MOTOR] + prev_encoderMOTOR) * (toRadians / dt); //it is better to make a int difference!!
    diffthetaE = (double)(-encodersPositionIn[LOAD] + prev_encoderLOAD) * (toRadians / dt); //it is better to make a int difference!!

    //2.period velocity

    //jerk
    jerkM = diffthetaM - prev_diffthetaM;
    jerkE = diffthetaE - prev_diffthetaE;


    //encoder period in second
    periodM = encodersPeriodIn[MOTOR] / 4e7; //4 step per pulse * 1e-7 time conversion
    periodE = encodersPeriodIn[LOAD] / 4e7; //4 step per pulse * 1e-7 time conversion

    //se la posizione rimane uguale incremento il periodo di dt
    if(thetaM==prev_thetaM) periodM += dt;
    if(thetaE==prev_thetaE) periodE += dt;

    //sign
	if(diffthetaM != 0) signM = diffthetaM > 0 ? 1.0 :-1.0;
	if(diffthetaE != 0) signE = diffthetaE > 0 ? 1.0 :-1.0;
	//mi sa che il rumore sullo zero rimane!! forse bisogna filtrare di nuovo la velocità. todo CHECK

    new_dthetaM = signM * (2*M_PI / ENCODER_STEPS) / periodM;
    new_dthetaE = signE * (2*M_PI / ENCODER_STEPS) / periodE;

    //period velocity update with outlier chek

	//outlier2: se ho un incremento improvviso > 10 lo cazzio subito. e mi avvicino lentamente al nuovo valore

	dthetaM_envelope = envelopeFilterM->process(fabs(prev_dthetaM));
    dthetaE_envelope = envelopeFilterE->process(fabs(prev_dthetaE));


	dthetaM = new_dthetaM;
	dthetaE = new_dthetaE;

	inc_dthetaM = fabs(dthetaM-prev_dthetaM);
	inc_dthetaE = fabs(dthetaE-prev_dthetaE);

//	//per i seni (o segnali periodici) è meglio questo
//	if( inc_dthetaM > 0.5*dthetaM_envelope) dthetaM = prev_dthetaM + sign(new_dthetaM-prev_dthetaM);
//	if( inc_dthetaE > 0.5*dthetaE_envelope) dthetaE = prev_dthetaE + sign(new_dthetaE-prev_dthetaE);

	//se no questo è generico
	if( inc_dthetaM > 10) dthetaM = prev_dthetaM + sign(new_dthetaM-prev_dthetaM);
	if( inc_dthetaE > 10) dthetaE = prev_dthetaE + sign(new_dthetaE-prev_dthetaE);

	//artificial noise
	i++;
	rand_amp = 0.0;
	randM = drand()*rand_amp - rand_amp/2;
	randE = drand()*rand_amp - rand_amp/2;
	dthetaM += randM;
	dthetaE += randE;

	//check if we are at stand still
	if(thetaM==prev_thetaM) isStandStillM++;
    else isStandStillM = 0;

    if(thetaE==prev_thetaE) isStandStillE++;
    else isStandStillE = 0;

	//acceleration
	ddiffthetaM = (dthetaM - prev_dthetaM) / dt;
	ddiffthetaE = (dthetaE - prev_dthetaE) / dt;



    //updates
	prev_encoderMOTOR = encodersPositionIn[MOTOR];
	prev_encoderLOAD = encodersPositionIn[LOAD];

	prev_thetaM = thetaM;
	prev_thetaE = thetaE;

	prev_dthetaM = dthetaM;
	prev_dthetaE = dthetaE;

	prev_diffthetaM = diffthetaM;//jerk
	prev_diffthetaE = diffthetaE;


}


bool BeckhoffSEAHardware::isStandStill(int times)
{
    if(isStandStillM>times && isStandStillE>times) return true;
    else return false;
}
