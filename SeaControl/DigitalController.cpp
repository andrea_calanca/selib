#include "DigitalController.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

double DigitalControl::__process(double y, double unused1, double unused2)
{
	return DigitalFilter::process(ref-y);
}
