#include "DigitalFilter.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

// class constructor
DigitalFilter::DigitalFilter(int filter_order, double* ap, double* bp)
{
	//to simplify things
    this->order = filter_order+1;

    for (int i=0;i<order;i++)
    {
        a[i] = ap[i];
        b[i] = bp[i];
        u[i] = 0.0;
        y[i] = 0.0;
    }
    k = 0;
}

void DigitalFilter::clean()
{
    for (int i=0;i<order;i++)
    {
        u[i] = 0.0;
        y[i] = 0.0;
    }
    k = 0;
}

double DigitalFilter::process(double input)
{
    int i,h=0;

    k++;
    k = k % order;

    u[k] =  input;
    y[k] =  b[0]*u[k];

	//matlab:
    //a(1)*y(n) = b(1)*x(n) + b(2)*x(n-1) + ... + b(nb+1)*x(n-nb) - a(2)*y(n-1) - ... - a(na+1)*y(n-na)
	//with our notation:
    //a(0)*y(k) = b(0)*u(k) + b(1)*u(k-1) + ... + b(nb)*x(k-nb)    - a(1)*y(k-1) - ... - a(na)*y(k-na)

    for (i=1;i<order;i++)
    {
        h = k - i + order;
        h = h % order;
        y[k] += b[i]*u[h] - a[i]*y[h];
    }

    y[k] = y[k] / a[0];

    return y[k];
}

DigitalFilter* DigitalFilter::getLowPassFilterHz(double f)
{
	double alpha = exp(-2*M_PI*f*TS);
	double b[2] = {(1-alpha),0.0}; //num
    double a[2] = {1.0,-alpha}; // den
    return new DigitalFilter(1,a,b);
}

DigitalFilter* DigitalFilter::getLowPassDifferentiatorHz(double f)
{
	double alpha = exp(-2*M_PI*f*TS);
	double b[2] = {f*(1-alpha),f*(alpha-1)}; //num
    double a[2] = {1.0,-alpha}; // den
    return new DigitalFilter(1,a,b);
}

//this is a 2nd order filter. Better or worse than simplectic?
DigitalFilter* DigitalFilter::getResonatorHz(double f,double r)
{
	int g;
	g = (1.0-r*r) / (1.0+r*r - 2.0*r*cos(2.0*M_PI*f*TS));
	double cb[3] = {(1-r*r)*0.5/g, 0.0, (1-r*r)*0.5/g}; //num
    double ca[3] = {1.0, - 2.0*r*cos(2.0*M_PI*f*TS), r*r}; // den
	return new DigitalFilter(2,ca,cb);
}

DigitalFilter* DigitalFilter::getDifferentiator()
{
    double b[2] = {1,-1}; //num
    double a[2] = {TS,0}; // den
    return new DigitalFilter(1,a,b);
}

////the following works at 3kHz sampling rate
//DigitalFilter* DigitalFilter::get20HzDerivator()
//{
//    double b[2] = {124.9,-124.9}; //num
//    double a[2] = {1.0,-0.9875}; // den
//    return new DigitalFilter(1,a,b);
//}
//
//DigitalFilter* DigitalFilter::get50HzDerivator()
//{
//    double b[2] = {309.3,-309.3}; //num
//    double a[2] = {1.0,-0.9691}; // den
//    return new DigitalFilter(1,a,b);
//}

DigitalFilter* DigitalFilter::getDelay0()
{
    double b[1] = {1}; //num
    double a[1] = {1}; // den
    return new DigitalFilter(0,a,b);
}

DigitalFilter* DigitalFilter::getDelay1()
{
    double b[2] = {0,1}; //num
    double a[2] = {1,0}; // den
    return new DigitalFilter(1,a,b);
}

DigitalFilter* DigitalFilter::getDelay2()
{
    double b[3] = {0,0,1}; //num
    double a[3] = {1,0,0}; // den
    return new DigitalFilter(2,a,b);
}

DigitalFilter* DigitalFilter::getDelay3()
{
    double b[4] = {0,0,0,1}; //num
    double a[4] = {1,0,0,0}; // den
    return new DigitalFilter(3,a,b);
}

DigitalFilter* DigitalFilter::getDelay4()
{
    double b[5] = {0,0,0,0,1}; //num
    double a[5] = {1,0,0,0,0}; // den
    return new DigitalFilter(4,a,b);
}

DigitalFilter* DigitalFilter::getDelay5()
{
    double b[6] = {0,0,0,0,0,1}; //num
    double a[6] = {1,0,0,0,0,0}; // den
    return new DigitalFilter(5,a,b);
}

DigitalFilter* DigitalFilter::getDelay10()
{
    double b[11] = {0,0,0,0,0,0,0,0,0,1}; //num
    double a[11] = {1,0,0,0,0,0,0,0,0,0}; // den
    return new DigitalFilter(10,a,b);
}

DigitalFilter* DigitalFilter::getMeanFilter()
{
	double n = 7;
    double b[7] = {1,1,1,1,1,1,1}; //num
    double a[7] = {n,0,0,0,0,0,0}; // den
    return new DigitalFilter(n,a,b);
}

//DigitalFilter* DigitalFilter::get005HzFilter()
//{
//    double b[2] = {0.0001047,0.0}; //num
//    double a[2] = {1.0,-0.9999}; // den
//    return new DigitalFilter(1,a,b);
//}
//
//DigitalFilter* DigitalFilter::get01HzFilter()
//{
//    double b[2] = {0.0002094,0.0}; //num
//    double a[2] = {1.0,-0.9998}; // den
//    return new DigitalFilter(1,a,b);
//}
//
//DigitalFilter* DigitalFilter::get05HzFilter()
//{
//    double b[2] = {0.001047,0.0}; //num
//    double a[2] = {1.0,-0.9990}; // den
//    return new DigitalFilter(1,a,b);
//}
//
//DigitalFilter* DigitalFilter::get1HzFilter()
//{
//    double b[2] = {0.002092,0.0}; //num
//    double a[2] = {1.0,-0.9979}; // den
//    return new DigitalFilter(1,a,b);
//}
//
//DigitalFilter* DigitalFilter::get5HzFilter()
//{
//    double b[2] = {0.01042,0.0}; //num
//    double a[2] = {1.0,-0.9896}; // den
//    return new DigitalFilter(1,a,b);
//}
//
//
//DigitalFilter* DigitalFilter::get10HzFilter()
//{
//    double b[2] = {0.02073,0.0}; //num
//    double a[2] = {1.0,-0.9793}; // den
//    return new DigitalFilter(1,a,b);
//}
//
//
//DigitalFilter* DigitalFilter::get100HzFilter()
//{
//    double b[2] = {0.189,0.0}; //num
//    double a[2] = {1.0,-0.811}; // den
//    return new DigitalFilter(1,a,b);
//}
//
//DigitalFilter* DigitalFilter::get50HzFilter()
//{
//    double b[2] = {0.09942,0.0}; //num
//    double a[2] = {1.0, - 0.9006}; // den
//    return new DigitalFilter(1,a,b);
//}
//
//DigitalFilter* DigitalFilter::get20HzFilter()
//{
//    double b[2] = {  0.04102,0.0}; //num
//    double a[2] = {1.0, -  0.959}; // den
//    return new DigitalFilter(1,a,b);
//}
//
//DigitalFilter* DigitalFilter::get30HzFilter()
//{
//    double b[2] = {  0.0609,0.0}; //num
//    double a[2] = {1.0, - 0.9391}; // den
//    return new DigitalFilter(1,a,b);
//}
//
//
//DigitalFilter* DigitalFilter::get1000HzFilter()
//{
//    double b[2] = {  0.8769,0.0}; //num
//    double a[2] = {1.0, - 0.1231}; // den
//    return new DigitalFilter(1,a,b);
//}


DigitalFilter* DigitalFilter::get10HzButtFilter()
{
    double b[2] = {0.0052,0.0052}; //num
    double a[2] = {1.0,-0.9896}; // den
    return new DigitalFilter(1,a,b);
}

DigitalFilter* DigitalFilter::get20HzButtFilter()
{
    double b[2] = {0.0104,0.0104}; //num
    double a[2] = {1.0,-0.9793}; // den
    return new DigitalFilter(1,a,b);
}

DigitalFilter* DigitalFilter::get100HzButtFilter()
{
    double b[2] = {0.0498,0.0498}; //num
    double a[2] = {1.0,-0.9004}; // den
    return new DigitalFilter(1,a,b);
}

DigitalFilter* DigitalFilter::get1000HzButtFilter()
{
    double b[2] = {0.3660,0.3660}; //num
    double a[2] = {1.0,-0.2679}; // den
    return new DigitalFilter(1,a,b);
}
