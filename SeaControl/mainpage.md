My Main Page                         {#mainpage}
============
The Series Elastic Library is a control library for Series Elastic Actuators (SEAs) written in c++ with a hierarchical reusable object oriented architecture. It implements state of the art force and impedance control algorithms for SEAs:

- %PID Force Control

- Passive Linear Force Control (PD + %PID with integral roll off)

- Passive Acceleration based Force Control [1][2]

- Passive Velocity sourced Force Control [3]

- Sliding-mode Force Control [4]

- Human Adaptive Force Control [5]

- Indirect Human Adaptive Force Control [6]

- Multi-model Human Adaptive Force Control [9] 

- Impedance contol based on all the above force implementations [7][8][9]

For an overview of the architecture take a look to the classes ControlBase and LoopTask. The easiest way to compile the project is to install codeblocks on a linux based OS and open SELib.cbp. Dependencies can be easily seen from the codeblocks environment. Also one need to implement a class with interface ISEAHardware to connect the SW with the HW, i.e. read sensor data and set motor currents. Notation and physical meaning of variables is according with figures 4 and 5 in [10] , see [here](https://www.researchgate.net/publication/265124731_On_The_Role_of_Compliance_In_Force_Control)

A beta release will be available soon on GitHub!! 

For any suggestion, question or need for improvements please contact andrea.calanca _at_ univr.it. 

References:

[1] G. A. Pratt and M. M. Williamson, Series Elastic Actuators, in International Conference on Intelligent Robots and Systems, 1995, vol. 1, pp. 399-406.

[2] M. M. Williamson, Series Elastic Actuators, Master Thesis, 1995.

[3] H. Vallery, R. Ekkelenkamp, H. van der Kooij, and M. Buss, Passive and accurate torque control of series elastic actuators, 2007 IEEE/RSJ Int. Conf. Intell. Robot. Syst., pp. 3534-3538, Oct. 2007.

[4] A. Calanca, L. Capisani, and P. Fiorini, Robust Force Control of Series Elastic Actuators, Actuators, Spec. Issue Soft Actuators, vol. 3, no. 3, pp. 182-204, 2014.

[5] A. Calanca and P. Fiorini, Human-Adaptive Control of Series Elastic Actuators, Robotica, vol. 2, no. 08, pp. 1301-1316, 2014.

[6] A. Calanca, R. Muradore, and P. Fiorini, A Study on the Passivity Human-Adaptive Control of Elastic Actuators, Submitt. to IJRR.

[7] H. Vallery, J. Veneman, E. H. F. van Asseldonk, R. Ekkelenkamp, M. Buss, and H. van Der Kooij, Compliant actuation of rehabilitation robots, IEEE Robot. Autom. Mag., vol. 15, no. 3, pp. 60-69, Sep. 2008.

[8] N. L. Tagliamonte and D. Accoto, Passivity Constraints for the Impedance Control of Series Elastic Actuators, J. Syst. Control Eng., vol. 228, no. 3, pp. 138-153, 2013.

[9] A. Calanca, R. Muradore, and P. Fiorini, Passive and Accurate Impedance Control of Series Elastic Actuators, Submitted, 2015.

[10] A. Calanca and P. Fiorini, On The Role of Compliance In Force Control, in International Conference on intelligent Autonomous Systems, 2014.