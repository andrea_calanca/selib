#ifndef HARDWARE_HPP
#define HARDWARE_HPP

#include "ethercatPlugin.hpp"
#include "DigitalFilter.hpp"

#include <math.h>
#include <sstream>
#include <cstring>
#include <vector>

#include"defines.h"
#include"ISEAHardware.hpp"

#define MOTOR 1
#define LOAD 0

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

using namespace std;

/**
This class implements the ISEAHardware interface to comunicate with the following Beckoff modules with
up to 3 KHz sampling time

EL5152 used to read the two encoders
EL4004 used to  write current set point
EL3102 used to read current + torque sensor
EL2202 used for digitalOutputs;
EL1202 used for digitalInputs;

The communication protocol is implemented in the classes ethercatPlugin and ELxxxxs and is based on the "Simple Open EtherCAT Master" (soem) project http://sourceforge.net/projects/soem.berlios/. The classes ethercatPlugin and ELxxxxs (not reported in this doxygen) are free for download and included in the SELib download file.
*/
class BeckhoffSEAHardware : public ISEAHardware, public EthercatPlugin
{

	public:
		~BeckhoffSEAHardware(){delete envelopeFilterM; delete envelopeFilterE;};
        BeckhoffSEAHardware();

        void refresh(double dt);


     	void setCurrent(double value) { motorsVoltageOut[0] = curSaturation(value) * SET_CURRENT_CONVERSION;}
        void setTauM(double value) { setCurrent(value/KT) ;}

        double getCurrent(){ return active_current; }
        double getTauM(){ return active_tau_m; }

        double getThetaM(){ return thetaM; }
        double getThetaE(){ return thetaE; }
        double getDThetaM(){ return dthetaM; }
        double getDThetaE(){ return dthetaE; }
        double getDiffThetaM(){ return diffthetaM; }
        double getDiffThetaE(){ return diffthetaE; }
        double getDDiffThetaM(){ return ddiffthetaM; }
        double getDDiffThetaE(){ return ddiffthetaE; }

		bool getIndexM(){ return digitalSensorIn[1]; }
        bool getIndexE(){ return digitalSensorIn[0]; }

        double getTorque(){ return torque;}
        double getDiffTorque(){ return difftorque;}

		void resetM(){ zeroMOTOR = encodersPositionIn[MOTOR]; prev_encoderMOTOR = zeroMOTOR; prev_thetaM = thetaM; envelopeFilterM->clean(); prev_diffthetaM = diffthetaM;}
		void resetE(){ zeroLOAD = encodersPositionIn[LOAD]; prev_encoderLOAD = zeroLOAD; prev_thetaE = thetaE;}

		bool isStandStill(int time);
		bool isSaturated(){return saturation_flag;};
		bool saturation_flag;

    private:
		int i;
		double rand_amp,randM,randE;

		double thetaM, prev_thetaM;
        double thetaE, prev_thetaE;
        double thetaMzero;
        double thetaEzero;

        int prev_encoderMOTOR, prev_encoderLOAD, zeroMOTOR, zeroLOAD;

        double dthetaM, diffthetaM, periodM, signM;
        double dthetaE, diffthetaE, periodE, signE;

        double jerkM, dthetaM_envelope, prev_diffthetaM, new_dthetaM, prev_dthetaM, inc_dthetaM, ddiffthetaM;
        double jerkE, dthetaE_envelope, prev_diffthetaE, new_dthetaE, prev_dthetaE, inc_dthetaE, ddiffthetaE;

        DigitalFilter* envelopeFilterM;
        DigitalFilter* envelopeFilterE;

        DigitalFilter* derivatorM;
        DigitalFilter* derivatorE;


        int isStandStillM, isStandStillE;

        double toRadians;

        double active_tau_m, active_current, torque, difftorque, prev_torque;

        double curSaturation(double in);

};

#endif
