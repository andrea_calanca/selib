#ifndef POSITIONTASK_H
#define POSITIONTASK_H

#include <math.h>
#include <sstream>
#include <string>

#include"LoopTask.hpp"
#include"defines.h"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

/**
This class implements a task to test position control algorithms
*/
class PositionTask: public LoopTask
{
public:
    PositionTask(ISEAHardware* hw);
    int _loop();
    void logOpen();
    void Log() { LogAll(); };
	double position_ref;
	MotorPID* ctr;
};


/**
This class implements a task to test velocity control algorithms
*/
class VelocityTask: public LoopTask
{
public:
    VelocityTask(ISEAHardware* hw);
    int _loop();
    void logOpen();
    void Log() { LogAll(); };
	double velocity_ref;
	MotorPID* ctr;
};

/**
This class implements a task to test position control with sinusoidal reference
*/
class SinPositionTask: public PositionTask
{
public:
	SinPositionTask(ISEAHardware* hw): PositionTask(hw){};
    int _loop();
    void logOpen();
    void Log() { LogAll(); };
    double amp;
    double freq;

protected:
	double fadeIn;
};

/**
This class implements a homing task
*/
class HomingTask: public PositionTask
{
public:
	HomingTask(ISEAHardware* hw): PositionTask(hw){}
    int _loop();
};




#endif // POSITIONTASK_H
