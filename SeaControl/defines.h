#ifndef DEFINES_H_INCLUDED
#define DEFINES_H_INCLUDED

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/


// ----------- GLOBAL VARIABLES ------------

//timing parameters
#define NSEC_PER_SEC    1000000000.0
#define RT_ETHERCAT_DELAY     70000 // 0.07 ms

//#define RT_INTERVAL     10000000 // 10 ms -> 100 Hz
//#define RT_INTERVAL     4000000 // 4 ms -> 250 Hz
//#define RT_INTERVAL     2000000 // 2 ms -> 500 Hz
//#define RT_INTERVAL     1000000 // 1 ms -> 1 kHz
//#define RT_INTERVAL     500000 // 0.5 ms -> 2 kHz
#define RT_INTERVAL     333333 // 0.333333 ms -> 3 kHz
//#define RT_INTERVAL     250000 // 0.25 ms -> 4 kHz
//#define RT_INTERVAL     200000 // 0.2 ms -> 5 kHz
//#define RT_INTERVAL     125000 // 0.125 ms -> 6 kHz NON CE LA FA
#define TS 0.0003333

//conversion parameters
#define ENCODER_STEPS 20000.0
#define SET_CURRENT_CONVERSION 1.4286 // 10 volt / 7 ampere
#define READ_CURRENT_CONVERSION  1.7500 //  7 ampere / 4 volt
#define READ_TORQUE_CONVERSION 2.5  // 25 Nm / 10 volt

//faulaber motor parameters
#define COULOMB_FRICTION_POS 0.09060
#define COULOMB_FRICTION_NEG 0.10880
#define VISCOUS_FRICTION_POS 0.01270
#define VISCOUS_FRICTION_NEG 0.01060

#define KT 1.43 // wrong2.28650
#define JM 0.02078
#define CUR_SAT 7

//JPL motor parameters
//#define COULOMB_FRICTION_POS 0.14411
//#define COULOMB_FRICTION_NEG 0.16078
//#define VISCOUS_FRICTION_POS 0.00211
//#define VISCOUS_FRICTION_NEG 0.00217

//#define KT 0.42375
//#define JM 0.00041
//#define CUR_SAT 3

#define TORQUE_SENSOR_OFFSET -0.0212

// spring-load parameters
//#define K_SPRING 1.03900 //metal soft spring
#define K_SPRING 2.4882 //metal hard spring
//#define K_SPRING 3.30890 //red spring
//#define K_SPRING 3580.99 //load cell

//#define JL 0.000125 //verylow
//#define JL 0.00068 //low (short wood)
//#define JL 0.00734 //low1 (long wood)
#define JL 0.00210 //mid
//#define JL 0.0307 //high
//#define JL 0.0099 //mid1
//#define JL 0.0868 //high1

#define A (JM/K_SPRING) // JM / K

// ------------- GLOBAL FUNCTIONS -------------

#include <stdlib.h>

#define sign(x) (signbit(x)?-1.0:1.0)
//#define sign(x)(x / fabs(x))

double inline drand() { return (double)rand() / (double)(RAND_MAX); }; //returns from 0 to 1

#endif // DEFINES_H_INCLUDED
