#ifndef OROCOS_COMPONENT_HPP
#define OROCOS_COMPONENT_HPP

/****************************************************************************
 * Copyright (C) 2015 Lorenzo Bertelli, Andrea Calanca
 * @author Lorenzo Bertelli, Andrea Calanca
 * @date june 2015
 ****************************************************************************/

#include "el4004.hpp"
#include "el5152.hpp"
#include "el3102.hpp"
#include "el2202.hpp"
#include "el3102.hpp"
#include "el1202.hpp"


#include <sstream>
#include <cstring>
#include <vector>

using namespace std;

class EthercatPlugin
{

    private:
        // Ethernet device used by EtherCAT master
        const char * ifname;

        // Input/Output EtherCATmapping
        char IOmap[4096];

        // EtherCAT modules
        EL2202 * digitalOutputs;
        EL1202 * digitalInputs;
        EL5152 * encoders;
        EL4004 * analogOutputs;
        EL3102 * analogInputs;

    public:
        EthercatPlugin(string const& name);

        bool configureHook();

        bool startHook();

        void updateHook();

        void stopHook();

        void cleanupHook();

        vector<double> motorsVoltageOut;
        vector<int> encodersPositionIn;
        vector<int> encodersPeriodIn;
        vector<double> analogSensorIn;
        vector<bool> digitalSensorIn;

};

#endif
