#include "MotorControl.hpp"


MAdaptiveForceControl::MAdaptiveForceControl(double l1, double l2, ISEAHardware* hw) : MotorControlBase(hw), ModelReference2(l1,l2)
{
	adaptationEnabled = 1;
	referenceModelEnabled = true;

    L = 2*M_PI*0.1;
    G = 100;

    ni = 0;
    phi = 5;

	a_est = 0.1;
	ddtheta0_est = 0;

	pid = new PID(0,5,0);


//	this->filename = "Madaptive.csv";

	this->filename = "Miadaptive.csv";


	w0filt = DigitalFilter::getLowPassFilterHz(5);
	w1filt = DigitalFilter::getLowPassFilterHz(5);
    w2filt = DigitalFilter::getLowPassFilterHz(5);

	anew_theta = Vector2d::Zero();
	theta = Vector2d::Zero();

	//gradient
	rho = Matrix2d::Zero();
	//LS
	P = Matrix2d::Identity(2,2);
	Q = Matrix2d::Identity(2,2);
	w = Vector2d::Zero();


	gradientUpdate = false;
	//speed
	//gradient
	rho(0,0) = G;
	rho(1,1) = G;
	//LS
	g = G;
	lambda = 0.999;
	ddtheta0Filter = DigitalFilter::getLowPassFilterHz(1);
}


void MAdaptiveForceControl::Log()
{
    logfile << ref << " ";
    logfile << tau << " ";
    logfile << dtau << " ";
    logfile << ref << " ";
    logfile << dref << " ";
    logfile << theta(0) << " ";
    logfile << theta(1) << " ";
    logfile << log1 << " ";
    logfile << log2 << " ";
    logfile << log3 << " ";
    logfile << this->time << " ";
    logfile << endl;

}

double MAdaptiveForceControl::__process(double theta, double dtheta, double dt)
{
//	//sliding parameters log
//    log1 = ni;
//	log2 = phi;
//	log3 = L;
//
//    if(referenceModelEnabled) MRUpdate(ref, dt);
//    else
//    {
//        xr1 = ref;
//        xr2 = dref;
//        f2 = ddref;
//    }
//
//    x_tilde	= tau - xr1;
//    dx_tilde = dtau - xr2;
//    nu = f2 - 0.1*L*dx_tilde - L*L*x_tilde;
//
//    s = dx_tilde + L*x_tilde;
////	s = L*x_tilde;
//
////    //sliding-mode robustification
////	us = sign(s);
////	if(fabs(s) < phi) us =  s / phi;
//
//    out = a_est * nu + tau - ni*us;

    //adaptivity
//    if(adaptationEnabled)
    adaptationUpdate(dt);

//    out += frictionCurrentCompensation(xr2) * KT;

	pid->ref = this->ref;
	pid->dref = this->dref;
	pid->ddref = this->ddref;
	return pid->process(tau,dtau,dt);
//    return out;

}

void MAdaptiveForceControl::adaptationUpdate(double dt)
{
	double ddtheta0, tau_motor,new_theta;
	ddtheta0 = ddthetaMFilter->process(ddtheta_m);
	ddtheta0 = 0;
	tau_motor = hw->getTauM() - frictionTorqueCompensation(dtheta_m);

//	//system identification
//    y = w0filt->process( (tau - tau_motor)/JM + ddtheta0  );
//    //Y = w0filt->process(out - A*ddtau);
//    w1 = w1filt->process(-1);
//    w2 = w2filt->process(ddtau);

	//system identification
    y = w0filt->process( tau  );
    w1 = w1filt->process( theta_m );
    w2 = w2filt->process( -1 );

    w << w1, w2;
    err = theta.dot(w) - y;

    if(gradientUpdate){
        //GRADIENT
        //dtheta = -rho*err*w;
        dtheta = -rho*err*w / (1+w.dot(w));
        theta = theta + dt*dtheta;
    }
    else{
		//LS
        dtheta = -g*P*w*err;
        //dP = -g*P*w*w.transpose()*P;			//(1)
        //dP = Q - g*P*w*w.transpose()*P;		//(2)
        dP = -g*P*w*w.transpose()*P + lambda*P;	//(3)


		if(theta(0) < 0.1)
		{
			theta(0) = 0.1;
			theta(1) = theta(0) * theta_m - tau;

			dtheta = dtheta*0;
			P = 0.1*Matrix2d::Identity(2,2);
//			Q = Matrix2d::Identity(2,2);
			dP = dP*0;
		}
//		else if(anew_theta(0) > 100.0)
//		{
//			theta(0) = 100;
//			dtheta = dtheta*0;
//			P = Matrix2d::Identity(2,2);
//			Q = Matrix2d::Identity(2,2);
////			dP = dP*0;
//		}

		P = P + dt*dP;
//        P = 0.5 * (P + P.transpose());          //p+pT / 2
        theta = theta + dt*dtheta;


    }



//	cout << "ddtheta0 " << theta(0);
//    cout << "1/ke " << theta(1);
//    if(theta(1) < 0.0) theta(1) = 0.0;

//	if(theta(0) < 0.1) theta(0) = 0.1;
//    else if (theta(0) > 100.0) theta(0) = 100.0;
	log1 = P(0,0);
    log2 = P(1,1);
    log3 = hw->getThetaE();

	cout << "ke " << theta(0);
    cout << " theta0 " << theta(1)/theta(0);
    cout << " P0 " << log1;
    cout << " P1 " << P(1,1);








//    b_est = theta(1);
//    c_est = theta(0);
////    da_est = - G * ( s*nu + 0*a_est );
//    a_est = a_est + dt*da_est;
//    cout << "a " << a_est;

//	if(b_est < 0.0) b_est = 0.0;
//	else if(b_est > A*2*L) b_est = A*2*L;
//   	log1 = hw->getThetaE();
//	log2 = hw->getThetaM();

}


//
//MAdaptiveForceControl::MAdaptiveForceControl(double l1, double l2, ISEAHardware* hw) : MotorControlBase(hw), ModelReference2(l1,l2)
//{
//	adaptationEnabled = 1;
//	referenceModelEnabled = true;
//
//    L = 2*M_PI*0.1;
//    G = 0.0000;
//
//    ni = 0;
//    phi = 5;
//
//	a_est = 0.1;
//	ddtheta0_est = 0;
//
//	this->filename = "Madaptive.csv";
//}
//
//
//void MAdaptiveForceControl::Log()
//{
//    logfile << ref << " ";
//    logfile << tau << " ";
//    logfile << dtau << " ";
//    logfile << xr1 << " ";
//    logfile << xr2 << " ";
//    logfile << a_est << " ";
//    logfile << a_est << " ";
//    logfile << log1 << " ";
//    logfile << log2 << " ";
//    logfile << log3 << " ";
//    logfile << this->time << " ";
//    logfile << endl;
//
//}
//
//double MAdaptiveForceControl::__process(double theta, double dtheta, double dt)
//{
//	//sliding parameters log
//    log1 = ni;
//	log2 = phi;
//	log3 = L;
//
//    if(referenceModelEnabled) MRUpdate(ref, dt);
//    else
//    {
//        xr1 = ref;
//        xr2 = dref;
//        f2 = ddref;
//    }
//
//    x_tilde	= tau - xr1;
//    dx_tilde = dtau - xr2;
//    nu = f2 - 0.1*L*dx_tilde - L*L*x_tilde;
//
//    s = dx_tilde + L*x_tilde;
////	s = L*x_tilde;
//
////    //sliding-mode robustification
////	us = sign(s);
////	if(fabs(s) < phi) us =  s / phi;
//
//    out = a_est * nu + tau - ni*us;
//
//    //adaptivity
//    if(adaptationEnabled) adaptationUpdate(dt);
//
////    out += frictionCurrentCompensation(xr2) * KT;
//    return out;
//
//}
//
//void MAdaptiveForceControl::adaptationUpdate(double dt)
//{
//    da_est = - G * ( s*nu + 0*a_est );
//    a_est = a_est + dt*da_est;
//    cout << "a " << a_est;
//
////	if(b_est < 0.0) b_est = 0.0;
////	else if(b_est > A*2*L) b_est = A*2*L;
////   	log1 = hw->getThetaE();
////	log2 = hw->getThetaM();
//
//}
