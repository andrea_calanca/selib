#ifndef TORQUECONTROLTASK_H
#define TORQUECONTROLTASK_H

#include <math.h>
#include <sstream>
#include <string>

#include "LoopTask.hpp"
#include"AdaptiveControl.hpp"
#include"SlidingMode.hpp"
#include"ImpedanceControl.hpp"
#include "PassiveControl.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

/**
This is an example class to test the implemented torque/force and impedance control algorithms. Control algortithms are initialized in the init method and are asked to follow a torque simusoidal reference in the main loop.
*/
class TorqueTask : public LoopTask
{
public:
	~TorqueTask();
	TorqueTask(ISEAHardware* hw);
    int _loop();
    double torque_ref, amp, freq;


protected:
    //controllers
    SEAControl* ctr;
    MRAdaptiveForceControl* mrac;
    SlidingModeForceControl* sm;
	IntegralSlidingModeForceControl* ism;
    SuperTwistingControl* stw;
    MRPDControl* mrpd;
    TrivialImpedanceControl* imp;
    BasicImpedanceControl* BIC;

    VelocitySourcedImpedanceControl* VSIC;
    AdaptiveImpedanceControl* MRACimp;
    SMImpedanceControl* SMimp;
    CollocatedAdmittanceControl* CAC;
    CollocatedImpedanceControl* CIC;

    PassivePIDControl* passivePid;
    PassivePrattForceControl* passivePratt;
    MRPassivePrattForceControl* mrPassivePratt;
    PassiveValleryForceControl* passiveVallery;
    IndirectAdaptiveForceControl* indirectAdaptive;
    MultiAdaptiveForceControl* MMadaptive;



    double k_des, d_des;
    double sat1, sat2;

    void LogTorque();
	void init(double lambda1, double lambda2);
private:
};

#endif // TORQUECONTROLTASK_H

