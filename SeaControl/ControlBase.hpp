#ifndef CONTROLLER_H
#define CONTROLLER_H

#include"defines.h"
#include <math.h>
#include <iostream>
#include <sstream>
#include "DigitalFilter.hpp"
#include <fstream>
#include "ISEAHardware.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

using namespace std;

/**
The class provides an cycle by cycle automatic logging functionality. Child class just need to override the member log() where the desred variable can be streamed to the iosteam logfile.
*/
class Loggable
{
public:
	bool logEnabled;
	string filename;

	Loggable() { logEnabled = false;}

    /**
    it can be overridden if some particular log initialization is necessary e.g. log headers or filename may depend on class parameters
    */
	virtual void logOpen() { if (logEnabled && !logfile.is_open()) logfile.open(filename.c_str());};

    void logClose() { if (logfile.is_open()) logfile.close();}
    virtual void Log() {}; ///< to be overridden!!


protected:
	ostringstream filenameStream;
	ofstream logfile;///< data must be stremed to this object when overriding the virtual method Log()

};

/**
This virtual class describes a generic Controller. The control law should be implemented by childs in the virtual _process function. The class also provides an automatic logging functionality by overriding the member log.
*/
class ControlBase: public Loggable
{
public:

	virtual double _process(double theta, double dtheta, double dt)=0;
    double process(double theta, double dtheta, double dt)
    {
    	this->time += dt;
		ret = _process(theta,dtheta,dt);
    	if (logEnabled && !logfile.is_open()) logOpen();
		if (logEnabled) Log();
    	return ret;
	}

    double ref, dref, ddref;

    void refSaturation( double lowThreshold, double upThreshold);

    inline static double saturation(double in, double threshold);
    inline static double saturation(double in,  double lowThreshold, double upThreshold);

protected:
    double out, time;
	double ret;
};

/**
This virtual class describes a controller for rigid joint i.e. a motro joint. The control law should be implemented by childs in the virtual __process function. The class provides motor position, velocity and torque members and an additional torque observer in the case the the torque sensor is missing. Also it provides friction compensation utilities.
*/
class MotorControlBase: public ControlBase
{
public:
	MotorControlBase(ISEAHardware *hw); ///< constructor. By default  accellerations are filtered (50Hz at Ts=0.333ms) while position and velocityes are not processed
	~MotorControlBase();

	virtual double __process(double theta, double dtheta, double dt)=0;

    double _process(double theta, double dtheta, double dt);

    double torqueObserver, diffTorqueObserver, torqueObserverCutOff, prev_torqueObserver, g;
	DigitalFilter* torqueObserverFilter; ///< preprocessing filter for the torque observer, set to 20Hz (at T=0.333ms) by default (low-pass, one pole)

	ISEAHardware *hw; ///< where to retrieve the sensor data

	double theta_m, dtheta_m, ddtheta_m;
	double tau, dtau, ddtau;


    DigitalFilter* thetaMFilter; ///< low-pass filter for thetaM, disabled (set to null) by default
    DigitalFilter* dthetaMFilter; ///< low-pass filter for dthetaM, disabled (set to null) by default
	DigitalFilter* ddthetaMFilter; ///< low-pass filter for ddthetaM, set to 20Hz  by default (one pole)
	DigitalFilter* torqueFilter; ///< low-pass filter for the torque read from the torque sensor, set to 20Hz by default (low-pass, one pole)
    DigitalFilter* dtorqueFilter; ///< low-pass filter for the numerical derivative of torque read from the torque sensor set to 20Hz by default
    DigitalFilter* ddtorqueFilter;///< low-pass filter for the numerical derivative of dtorque set to 20Hz by default (one pole)


	static double frictionCurrentCompensationColoumb(double velocity)
    { return signbit(velocity) ? -COULOMB_FRICTION_NEG: COULOMB_FRICTION_POS;}

    static double frictionTorqueCompensationColoumb(double velocity)
    { return frictionCurrentCompensationColoumb(velocity) * KT;}

    static double frictionCurrentCompensation(double velocity)
    { return signbit(velocity) ? -COULOMB_FRICTION_NEG - VISCOUS_FRICTION_NEG*velocity : COULOMB_FRICTION_POS + VISCOUS_FRICTION_NEG*velocity;}

    static double frictionTorqueCompensation(double velocity)
    { return frictionCurrentCompensation(velocity) * KT;}
};



/**
This is the abstract base class for control algorithms for elastic joints, namely SEAs (series elastic actuators). It contains the basic SEA states including the spring torque (tau), the motor position (theta_m) and the environment position (theta_e) with first and second order derivatives. All these variables can be optionally filtered. This class retirieve the signal from the hw so it needs a pointer to an object which implements the ISEAHardware interface.Child classes must override the member ___process
*/
class SEAControl: public MotorControlBase
{
public:
    SEAControl(ISEAHardware *hw); ///< constructor. By default  accellerations are filtered (50Hz at Ts=0.333ms) while position and velocityes are not processed
	~SEAControl();

	virtual double ___process(double dt)=0; ///< virtual method that implements the specific SEA control law
	double __process(double theta, double dtheta, double dt); ///< reads the sensor data from ISEAHardware and sets their pre-processing, then invokes ___process. Modify this method for changing the pre-processing. The frist two parameters (theta and dthea) are not used (sensor data is directly read from ISEAHardware).

	bool torqueFromDisplacement; ///< set true to overwrite the force sensor data

	double theta_e, dtheta_e, ddtheta_e;

    DigitalFilter* thetaEFilter; ///< preprocessing filter for thetaE, disabled (set to null) by default
    DigitalFilter* dthetaEFilter; ///< preprocessing filter for dthetaE, disabled (set to null) by default
    DigitalFilter* ddthetaEFilter; ///< preprocessing filter for ddthetaE, set to 50Hz (at T=0.333ms) by default (low-pass, one pole)


};


/**
A PID controller with a velocity filter an a output stage filter (before the control output)
*/
class PID: public ControlBase
{
public:
    PID(double ki, double kp, double kd);
    double _process(double theta, double dtheta, double deltaTime);
    double KP, KD, KI, integralSAT;

    DigitalFilter* outFilter; ///< output filter. Set to null (default) to skip it
    DigitalFilter* velFilter; ///< velocity filter. Set to null (default) to skip it

private:
    double err, derr, ierr;

};

/**
A PID algorithm for motor control. It uses the class PID by composition
*/
class MotorPID: public MotorControlBase
{
public:
    MotorPID(double ki, double kp, double kd, ISEAHardware* hw);
    double __process(double pos, double vel, double dt);
    PID* pid;
};




#endif // CONTROLLER_H
