#ifndef CIMPEDANCECONTROL_H
#define CIMPEDANCECONTROL_H

#include "ControlBase.hpp"
// #include "SlidingMode.hpp"
// #include "AdaptiveControl.hpp"
// #include "PassiveControl.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

/**
Base class for impedance control algorithms. It provides desired stifness and damping proprierties and common log fonctionality for child impedance control algorithms (common file naming and data streaming)
*/
class ImpedanceControlBase: public SEAControl
{
	public:
		ImpedanceControlBase(ISEAHardware* hw);
		void Log();
		double k_des; ///< desired stiffness
		double d_des; ///< desired damping
		void logOpen();
    protected:
		double log1, log2, log3;
};

/**
Under test - works only for direct drive motor
*/
class SimpleImpedanceControl: public ImpedanceControlBase
{
	public:
		SimpleImpedanceControl(ISEAHardware* hw) : ImpedanceControlBase(hw) {}
		double __process(double dt);
	private:
};

/**
This class implements impedance control based on an inner PD force control, using the PID class.
*/
class PDImpedanceControl: public ImpedanceControlBase
{
	public:
//		PDImpedanceControl(ISEAHardware* hw) : ImpedanceControlBase(hw), pid(0,10,1) { 	this->filename = "impedancePDlow"; };
//		PDImpedanceControl(ISEAHardware* hw) : ImpedanceControlBase(hw), pid(0,200,1) { 	this->filename = "impedancePDhigh"; };
//		PDImpedanceControl(ISEAHardware* hw) : ImpedanceControlBase(hw), pid(0,200,0.1) { 	this->filename = "impedancePDhigh1"; };//unstable
		PDImpedanceControl(ISEAHardware* hw) : ImpedanceControlBase(hw), pid(0,50,1) { 	this->filename = "impedancePDmid"; };
		double __process(double dt);
	PID pid;
};


/**
This class implements impedance control based on an inner PD force control, using the PID class.
*/
class CollocatedImpedanceControl: public ImpedanceControlBase
{
	public:
//		CollocatedImpedanceControl(ISEAHardware* hw) : ImpedanceControlBase(hw), pid(0,10,1) { 	this->filename = "impedancePDlow"; };
//		CollocatedImpedanceControl(ISEAHardware* hw) : ImpedanceControlBase(hw), pid(0,200,1) { 	this->filename = "impedancePDhigh"; };
//		CollocatedImpedanceControl(ISEAHardware* hw) : ImpedanceControlBase(hw), pid(0,200,0.1) { 	this->filename = "impedancePDhigh1"; };//unstable
		CollocatedImpedanceControl(ISEAHardware* hw) : ImpedanceControlBase(hw), pid(0,50,1) { 	this->filename = "collocated_impedance_PDmid"; };
		double __process(double dt);
	PID pid;
};

/**
This class implements impedance control based on an inner PD force control, using the PID class.
*/
class CollocatedAdmittanceControl: public ImpedanceControlBase
{
	public:
		CollocatedAdmittanceControl(ISEAHardware* hw) : ImpedanceControlBase(hw), pid(0,50,1) { 	this->filename = "collocated_admittance_PDmid"; };
		double __process(double dt);
	PID pid;
};

/**
This class implements impedance control based on an inner adaptive force control.
*/
class AdaptiveImpedanceControl: public ImpedanceControlBase
{
	public:
		AdaptiveImpedanceControl(ISEAHardware* hw); ///< Edit here to set the default adaptive algorithm
		double __process(double dt);
		AdaptiveForceControlBase* c;

};

/**
This class implements impedance control based on an inner sliding-mode force control
*/
class SMImpedanceControl: public ImpedanceControlBase
{
	public:
		SMImpedanceControl(ISEAHardware* hw); ///< Edit here set the specific SM algorithm
		double __process(double dt);
		SlidingModeControlBase* c;
};


/**
This class implements impedance control based on an passive force control with inner motor velocity feedback. Passivity analisis of this algorithm is reported in

H. Vallery, J. Veneman, E. H. F. van Asseldonk, R. Ekkelenkamp, M. Buss, and H. van Der Kooij, “Compliant actuation of rehabilitation robots,” IEEE Robot. Autom. Mag., vol. 15, no. 3, pp. 60–69, Sep. 2008.
*/
class ValleryImpedanceControl: public ImpedanceControlBase
{
	public:
		ValleryImpedanceControl(ISEAHardware* hw); ///< Edit here set the specific SM algorithm
		double __process(double dt);
		PassiveValleryForceControl* c;
};


#endif // SEAIMPEDANCECONTROLLER_H
