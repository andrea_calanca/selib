#ifndef FORCETASK_H
#define FORCETASK_H

#include <math.h>
#include <sstream>
#include <string>

#include"defines.h"
#include"LoopTask.hpp"
#include"AdaptiveControl.hpp"
#include"MotorControl.hpp"


/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/




/**
This class implements a task to test force control algorithms
*/
class ForceTask : public LoopTask
{
public:
    ForceTask(ISEAHardware* hw);
    int _loop();
    void logOpen();
    void Log();
	double position_ref;

	MotorPID* pd;

	MAdaptiveForceControl* mrac;

	MotorControlBase* ctr;
};


#endif // FORCETASK_H
