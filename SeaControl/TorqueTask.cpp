#include "TorqueTask.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/


LoopTask* _rr_create_task(ISEAHardware* hw)
{
    TorqueTask* tc = new TorqueTask(hw);
    tc->filename = "provatorq";
    tc->maxDuration = 120.0;
    tc->logEnabled = false;
    tc->torque_ref = 0.01;
    return tc;
}


TorqueTask::~TorqueTask()
{
	delete mrac;
	delete mrpd;
	delete ism;
	delete stw;
	delete imp;
	delete BIC;
	delete MRACimp;
	delete SMimp;
	delete passivePid;
	delete passivePratt;
	delete mrPassivePratt;
	delete passiveVallery;
	delete indirectAdaptive;
	delete VSIC;
};

void TorqueTask::init(double lambda1, double lambda2)
{
	//-------- adaptive ---------------------------
	mrac = new MRAdaptiveForceControl(lambda1,lambda2,hw);
    mrac->logEnabled = true;
    mrac->referenceModelEnabled = true;

	mrpd = new MRPDControl(lambda1,lambda2,hw);
	mrpd->kp = 30;
    mrpd->kd = 0.01; //0.1
    mrpd->ki = 0; //1.0
	mrpd->logEnabled = true;

	indirectAdaptive = new IndirectAdaptiveForceControl(lambda1,lambda2,hw);
	indirectAdaptive->logEnabled = true;

	MMadaptive = new MultiAdaptiveForceControl(lambda1,lambda2,hw);
	MMadaptive->logEnabled = true;

	//-----sliding-modes ---------------

	sm = new SlidingModeForceControl(lambda1,hw);
	sm->ni = 0.5;
	sm->logEnabled = true;

	ism = new IntegralSlidingModeForceControl(lambda1,lambda2,hw);
	ism->ni = 1;
	ism->phi = 10;
	ism->logEnabled = true;

	stw = new SuperTwistingControl(lambda1,hw);
	stw->ni = 0.2;
	stw->logEnabled = true;

	//---------Passive-----------------------------------

	passivePid = new PassivePIDControl(hw);
	passivePid->logEnabled = true;

	passivePratt = new PassivePrattForceControl(lambda1,lambda2,hw);
	passivePratt->logEnabled = true;

	mrPassivePratt = new MRPassivePrattForceControl(lambda1,lambda2,hw);
//	mrPassivePratt->l1 = lambda1;
//	mrPassivePratt->l2 = lambda2;
	mrPassivePratt->logEnabled = true;

	passiveVallery = new PassiveValleryForceControl(hw);
	passiveVallery->logEnabled = true;

	//----------Impedance----------------------------------

	imp = new TrivialImpedanceControl(hw);
	imp->logEnabled = true;

	BIC = new BasicImpedanceControl(hw);
	BIC->logEnabled = true;

	MRACimp = new AdaptiveImpedanceControl(hw);
	MRACimp->logEnabled = true;

	SMimp = new SMImpedanceControl(hw);
	SMimp->logEnabled = true;

	VSIC = new VelocitySourcedImpedanceControl(hw);
	VSIC->logEnabled = true;

	CIC = new CollocatedImpedanceControl(hw);
	CIC->logEnabled = true;

	CAC = new CollocatedAdmittanceControl(hw);
	CAC->logEnabled = true;
}

TorqueTask::TorqueTask(ISEAHardware* hw): LoopTask(hw)
{
	//----- tuning ---------------
	double lambda1, lambda2, f;

	f = 20;

	lambda2 = powf(2*M_PI*f,2);
//	lambda2 = 1/A; //sliding
	lambda1 = 2.0*sqrt(lambda2); //diminuire questo toglie il chattering

	init(lambda1,lambda2);

    //------- set the active controller and reference ------------

//	passivePratt->kb = 1.0;
//	mrPassivePratt->kb = 0.9;
//    mrac->ni = 0.0; //migliori risultati tra 0.5 e 1
//	  mrac->G = 20;

	indirectAdaptive->ni = 0.0; //migliori risultati tra 0.5 e 1
	indirectAdaptive->setAdaptationSpeed(10);
	indirectAdaptive->gradientUpdate = true;

//	mrac->setAdaptationSpeed(100);

//	ctr = ism;
//	ctr = sm;
//  ctr = passivePratt;
//	ctr = mrPassivePratt;
	ctr = passivePid;
//  ctr = mrac;
//	ctr = mrpd;
//  ctr = imp;
//	ctr = indirectAdaptive;
//	ctr = MMadaptive;
//	ctr = passiveVallery;


//ctr = MRACimp;

//	ctr = VSIC;
//	ctr = BIC;
//	ctr = CAC;
//	ctr = CIC;
//	ctr = SMimp;



	SMimp->c->ni = 0.0;
//    endLine = false;
//    mrac->adaptationEnabled = 0;
//    mrac->c_est = 18;
//    mrac->b_est = 0;

    amp = 0.1;
    freq = 1;

//	ctr->logEnabled = false;

	k_des = 1.5*K_SPRING;
	d_des = 0.0;


	//impedance control
	imp->k_des = k_des;
	imp->d_des = d_des;
	BIC->k_des = k_des;
	BIC->d_des = d_des;
	MRACimp->k_des = k_des;
	MRACimp->d_des = d_des;
	SMimp->k_des = k_des;
	SMimp->d_des = d_des;
	VSIC->k_des = k_des;
	VSIC->d_des = d_des;
	CIC->k_des = k_des;
	CIC->d_des = d_des;
	CAC->k_des = k_des;
	CAC->d_des = d_des;

}


int TorqueTask::_loop()
{

//	//SIN
//	//ctr->ref = amp + amp*sin(2 * M_PI * freq * time);
//	ctr->ref = amp*sin(2 * M_PI * freq * time);
//	ctr->dref =  amp* 2 * M_PI * freq * cos(2 * M_PI * freq * time);
//	ctr->ddref =  - amp * powf(2 * M_PI * freq,2) * sin(2 * M_PI * freq * time);

//	STEP
	ctr->ref = -1;
	ctr->dref = 0;
	ctr->ddref = 0;

//	cout << " tau_current:" << hw->getCurrent() * KT << " tau_c:" << hw->getTauM() << " tau_s:" << ctr->tau << " tau:" << hw->getTorque();
	cout << " tau_current:" << hw->getCurrent() * KT << " tau_s:" << ctr->tau << " tau:" << hw->getTorque();

    current = ctr->process(0.0,0.0,dt) / KT; // I SEA controllers  already know dtau & dtau

    hw->setCurrent(current);



    return 1;
}

void TorqueTask::LogTorque()
{
    logfile << time << " ";
    logfile << hw->getCurrent() << " ";
    logfile << current << " ";
    logfile << endl;
}
