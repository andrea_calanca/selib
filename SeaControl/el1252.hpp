#ifndef EL1252_HPP
#define EL1252_HPP
#include "elXXXX.hpp"

// SOEM includes
extern "C" {
#include "ethercattype.h"
#include "nicdrv.h"
#include "ethercatbase.h"
#include "ethercatmain.h"
#include "ethercatcoe.h"
#include "ethercatfoe.h"
#include "ethercatconfig.h"
#include "ethercatprint.h"
}


using namespace std;

class EL1252 : public ELXXXX
{
    public:
        EL1252(int slaveID, ec_slavet * ec_slave);

        void getInput(int channel);
};

#endif
