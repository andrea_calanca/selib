/****************************************************************************
 * Copyright (C) 2015 Lorenzo Bertelli, Andrea Calanca
 * @author Lorenzo Bertelli, Andrea Calanca
 * @date june 2015
 ****************************************************************************/

#include <iostream>
#include <stdio.h>

#include "ethercatPlugin.hpp"

// SOEM includes
extern "C" {
#include "ethercattype.h"
#include "nicdrv.h"
#include "ethercatbase.h"
#include "ethercatmain.h"
#include "ethercatcoe.h"
#include "ethercatfoe.h"
#include "ethercatconfig.h"
#include "ethercatprint.h"
}

EthercatPlugin::EthercatPlugin(string const& name) :
    motorsVoltageOut(2, 0),
    encodersPositionIn(2,0),
    encodersPeriodIn(2,0),
    analogSensorIn(2,0),
    digitalSensorIn(2,0)
{
    // TODO: add connection here?
}


bool EthercatPlugin::configureHook()
{
    try{
        // TODO: add configuration commands?
        ifname = "eth0";

		digitalInputs = new EL1202(2, ec_slave);
		digitalOutputs = new EL2202(3, ec_slave);
        encoders = new EL5152(4, ec_slave);
        analogOutputs = new EL4004(5, ec_slave);
        analogInputs = new EL3102(6, ec_slave);

        return true;
    }catch(string e){
        cout << "ERROR: " << e << endl;
        return false;
    }
}


bool EthercatPlugin::startHook()
{
    try
    {
        // initialise SOEM, bind socket to ifname
        if (ec_init(ifname))
        {
            cout << "ec_init on " << ifname << " succeeded." << endl;
            // find and auto-config slaves


            if ( ec_config_init(FALSE) > 0 )
            {
                cout << ec_slavecount << " slaves found and configured." << endl;

                ec_config_map(&IOmap);

                cout << "Slaves mapped, state to SAFE_OP." << endl;
                // wait for all slaves to reach SAFE_OP state
                ec_statecheck(0, EC_STATE_SAFE_OP,  EC_TIMEOUTSTATE * 4);

                cout << "Request operational state for all slaves." << endl;
                cout << "Calculated workcounter " << ec_group[0].expectedWKC << endl;
                ec_slave[0].state = EC_STATE_OPERATIONAL;
                // send one valid process data to make outputs in slaves happy
                ec_send_processdata();
                ec_receive_processdata(EC_TIMEOUTRET);
                // request OP state for all slaves
                ec_writestate(0);
                // wait for all slaves to reach OP state
                ec_statecheck(0, EC_STATE_OPERATIONAL,  EC_TIMEOUTSTATE * 4);
                if (ec_slave[0].state == EC_STATE_OPERATIONAL )
                {
                    cout << "Operational state reached for all slaves." << endl;

                    if (encoders->resetEncoders())
                    {
                    	digitalOutputs->initOutput();
                    	digitalOutputs->setOutput(1, true);
                        return true;
                    }
                    else
                    {
                        cout << "Encoder reset failed! " << endl;
                        return false;
                    }
                }
                else
                {
                    cout << "Operational state NOT reached for all slaves." << endl;
                }
            }
        }
        return false;

    }
    catch(string e)
    {
        cout << e << endl;
        return false;
    }
}

void EthercatPlugin::updateHook()
{
    int slave;
    int wkc;

    try
    {
        if (ec_slavecount > 0)
        {
            // Put here the data to be SENT
            digitalOutputs->setOutput(1, true);
            analogOutputs->setAnalog_1_2(motorsVoltageOut[0]);
            analogOutputs->setAnalog_3_4(motorsVoltageOut[1]);
            // end

            // Send and receive process data
            ec_send_processdata();
            // Parse the received process data
            wkc = ec_receive_processdata(EC_TIMEOUTRET);

            if( (wkc < ec_group[0].expectedWKC) || ec_group[0].docheckstate)
            {
                // one ore more slaves are not responding
                ec_group[0].docheckstate = FALSE;
                ec_readstate();
                for (slave = 1; slave <= ec_slavecount; slave++)
                {
                    if (ec_slave[slave].state != EC_STATE_OPERATIONAL)
                    {
                        ec_group[0].docheckstate = TRUE;
                        if (ec_slave[slave].state == (EC_STATE_SAFE_OP + EC_STATE_ERROR))
                        {
                            cout << "ERROR : slave " << slave << " is in SAFE_OP + ERROR, attempting ack." << endl;
                            ec_slave[slave].state = (EC_STATE_SAFE_OP + EC_STATE_ACK);
                            ec_writestate(slave);
                        }
                        else if(ec_slave[slave].state == EC_STATE_SAFE_OP)
                        {
                            cout << "WARNING : slave " << slave << " is in SAFE_OP, change to OPERATIONAL." << endl;
                            ec_slave[slave].state = EC_STATE_OPERATIONAL;
                            ec_writestate(slave);
                        }
                        else if(ec_slave[slave].state > 0)
                        {
                            if (ec_reconfig_slave(slave))
                            {
                                ec_slave[slave].islost = FALSE;
                                cout << "MESSAGE : slave " << slave << " reconfigured" << endl;
                            }
                        }
                        else if(!ec_slave[slave].islost)
                        {
                            ec_slave[slave].islost = TRUE;
                            cout << "ERROR : slave " << slave << " lost" << endl;
                        }
                    }
                    if (ec_slave[slave].islost)
                    {
                        if(!ec_slave[slave].state)
                        {
                            if (ec_recover_slave(slave))
                            {
                                ec_slave[slave].islost = FALSE;
                                cout << "MESSAGE : slave " << slave << " recovered" << endl;
                            }
                        }
                        else
                        {
                            ec_slave[slave].islost = FALSE;
                            cout << "MESSAGE : slave " << slave << " found" << endl;
                        }
                    }
                }
                if(!ec_group[0].docheckstate)
                    cout << "OK : all slaves resumed OPERATIONAL." << endl;
            }
            else
            {
                // Put here the received data assignements
                encodersPositionIn[0] = encoders->getPosition1();
                encodersPositionIn[1] = encoders->getPosition2();

                encodersPeriodIn[0] = encoders->getPeriod1();
                encodersPeriodIn[1] = encoders->getPeriod2();

                analogSensorIn[0] = analogInputs->getAnalog1();
                analogSensorIn[1] = analogInputs->getAnalog2();

                digitalSensorIn[0] = digitalInputs->getInput1();
                digitalSensorIn[1] = digitalInputs->getInput2();


                // end
/*
                cout << "ENC IN: " << encoders->getInputsAsString() << endl;
                cout << "ENC OUT: " << encoders->getOutputsAsString() << endl;
                cout << "AOUT IN: " << analogOutputs->getInputsAsString() << endl;
                cout << "AOUT OUT: " << analogOutputs->getOutputsAsString() << endl;
                cout << "AIN IN: " << analogInputs->getInputsAsString() << endl;
                cout << "AIN OUT: " << analogInputs->getOutputsAsString() << endl << endl;

                cout << "P1: " << encoders->getPeriod1() << " P2: " << encoders->getPeriod2() << endl << endl;
*/

//                cout << "DIG IN: " << digitalInputs->getInputsAsString() << " ";
//                cout << "DIG OU: " << digitalInputs->getOutputsAsString() << endl;

            }
        }

    }
    catch(string e)
    {
        cout << e << endl;
    }
}

void EthercatPlugin::stopHook()
{
    // TODO: add stopping code

    cout << "EthercatPlugin executes stopping !" << endl;
}

void EthercatPlugin::cleanupHook()
{
  cout << "EthercatPlugin cleaning up !" << endl;
}
