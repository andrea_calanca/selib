/** \file
 * \brief Example code for Simple Open EtherCAT master
 *
 * Usage : simple_test [ifname1]
 * ifname is NIC interface, f.e. eth0
 *
 * This is a minimal test.
 *
 * (c)Arthur Ketels 2010 - 2011
 */

#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>

#include "ethercattype.h"
#include "nicdrv.h"
#include "ethercatbase.h"
#include "ethercatmain.h"
#include "ethercatcoe.h"
#include "ethercatfoe.h"
#include "ethercatconfig.h"
#include "ethercatprint.h"

char usdo[128];
char hstr[1024];
char IOmap[4096];

char* SDO2string(uint16 slave, uint16 index, uint8 subidx, uint16 dtype)
{
	int l = sizeof(usdo) - 1, i;
	uint8 *u8;
	int8 *i8;
	uint16 *u16;
	int16 *i16;
	uint32 *u32;
	int32 *i32;
	uint64 *u64;
	int64 *i64;
	float *sr;
	double *dr;
	char es[32];

	memset(&usdo, 0, 128);
	ec_SDOread(slave, index, subidx, FALSE, &l, &usdo, EC_TIMEOUTRXM);
	if (EcatError)
	{
		return ec_elist2string();
	}
	else
	{
		switch(dtype)
		{
			case ECT_BOOLEAN:
				u8 = (uint8*) &usdo[0];
				if (*u8) sprintf(hstr, "TRUE"); 
				 else sprintf(hstr, "FALSE");
				break;
			case ECT_INTEGER8:
				i8 = (int8*) &usdo[0];
				sprintf(hstr, "0x%2.2x %d", *i8, *i8); 
				break;
			case ECT_INTEGER16:
				i16 = (int16*) &usdo[0];
				sprintf(hstr, "0x%4.4x %d", *i16, *i16); 
				break;
			case ECT_INTEGER32:
			case ECT_INTEGER24:
				i32 = (int32*) &usdo[0];
				sprintf(hstr, "0x%8.8x %d", *i32, *i32); 
				break;
			case ECT_INTEGER64:
				i64 = (int64*) &usdo[0];
				sprintf(hstr, "0x%16.16llx %lld", *i64, *i64); 
				break;
			case ECT_UNSIGNED8:
				u8 = (uint8*) &usdo[0];
				sprintf(hstr, "0x%2.2x %u", *u8, *u8); 
				break;
			case ECT_UNSIGNED16:
				u16 = (uint16*) &usdo[0];
				sprintf(hstr, "0x%4.4x %u", *u16, *u16); 
				break;
			case ECT_UNSIGNED32:
			case ECT_UNSIGNED24:
				u32 = (uint32*) &usdo[0];
				sprintf(hstr, "0x%8.8x %u", *u32, *u32); 
				break;
			case ECT_UNSIGNED64:
				u64 = (uint64*) &usdo[0];
				sprintf(hstr, "0x%16.16llx %llu", *u64, *u64); 
				break;
			case ECT_REAL32:
				sr = (float*) &usdo[0];
				sprintf(hstr, "%f", *sr); 
				break;
			case ECT_REAL64:
				dr = (double*) &usdo[0];
				sprintf(hstr, "%f", *dr); 
				break;
			case ECT_BIT1:
			case ECT_BIT2:
			case ECT_BIT3:
			case ECT_BIT4:
			case ECT_BIT5:
			case ECT_BIT6:
			case ECT_BIT7:
			case ECT_BIT8:
				u8 = (uint8*) &usdo[0];
				sprintf(hstr, "0x%x", *u8); 
				break;
			case ECT_VISIBLE_STRING:
				strcpy(hstr, usdo);
				break;
			case ECT_OCTET_STRING:
				hstr[0] = 0x00;
				for (i = 0 ; i < l ; i++)
				{ 
					sprintf(es, "0x%2.2x ", usdo[i]);
					strcat( hstr, es);
				}
				break;
			default:
				sprintf(hstr, "Unknown type");
		}
		return hstr;
	}
}

void simpletest(char *ifname)
{
	int i, j, oloop, iloop, wkc, wkc_count, slave;
    int counter = 0;
	int SDOsize = sizeof(usdo) - 1;
	int32 *i32;
    uint16 uword;
	boolean needlf;

	needlf = FALSE;
	printf("Starting simple test\n");
	
	/* initialise SOEM, bind socket to ifname */
	if (ec_init(ifname))
	{	
		printf("ec_init on %s succeeded.\n",ifname);
		/* find and auto-config slaves */


	    if ( ec_config_init(FALSE) > 0 )
		{
			printf("%d slaves found and configured.\n",ec_slavecount);

//            int Osize = 0, Isize = 0;
//            if (ec_readPDOmap(2, &Osize, &Isize) > 0)
//    			printf("PDO map ok. O=%d I =%d\n", Osize, Isize);
			ec_config_map(&IOmap);

			printf("Slaves mapped, state to SAFE_OP.\n");
			/* wait for all slaves to reach SAFE_OP state */
			ec_statecheck(0, EC_STATE_SAFE_OP,  EC_TIMEOUTSTATE * 4);

			oloop = ec_slave[0].Obytes;
			if ((oloop == 0) && (ec_slave[0].Obits > 0)) oloop = 1;
			//if (oloop > 8) oloop = 8;
			iloop = ec_slave[0].Ibytes;
			if ((iloop == 0) && (ec_slave[0].Ibits > 0)) iloop = 1;
			//if (iloop > 8) iloop = 8;

			printf("segments : %d : %d %d %d %d\n",ec_group[0].nsegments ,ec_group[0].IOsegment[0],ec_group[0].IOsegment[1],ec_group[0].IOsegment[2],ec_group[0].IOsegment[3]);

			printf("Request operational state for all slaves\n");
			printf("Calculated workcounter %d\n",ec_group[0].expectedWKC);
			ec_slave[0].state = EC_STATE_OPERATIONAL;
			/* send one valid process data to make outputs in slaves happy*/
			ec_send_processdata();
			ec_receive_processdata(EC_TIMEOUTRET);
			/* request OP state for all slaves */
			ec_writestate(0);
			/* wait for all slaves to reach OP state */
			ec_statecheck(0, EC_STATE_OPERATIONAL,  EC_TIMEOUTSTATE * 4);
			if (ec_slave[0].state == EC_STATE_OPERATIONAL )
			{
				printf("Operational state reached for all slaves.\n");
				wkc_count = 0;
				/* cyclic loop 10 times */
				for(i = 1; i <= 10000; i++)
				{
					ec_send_processdata();
					wkc = ec_receive_processdata(EC_TIMEOUTRET);

					if( (wkc < ec_group[0].expectedWKC) || ec_group[0].docheckstate)
					{
						if (needlf)
						{
							needlf = FALSE;
							printf("\n");
						}
						/* one ore more slaves are not responding */
						ec_group[0].docheckstate = FALSE;
						ec_readstate();
						for (slave = 1; slave <= ec_slavecount; slave++)
						{
							if (ec_slave[slave].state != EC_STATE_OPERATIONAL)
							{
								ec_group[0].docheckstate = TRUE;
								if (ec_slave[slave].state == (EC_STATE_SAFE_OP + EC_STATE_ERROR))
								{
									printf("ERROR : slave %d is in SAFE_OP + ERROR, attempting ack.\n", slave);
									ec_slave[slave].state = (EC_STATE_SAFE_OP + EC_STATE_ACK);
									ec_writestate(slave);
								}
								else if(ec_slave[slave].state == EC_STATE_SAFE_OP)
								{
									printf("WARNING : slave %d is in SAFE_OP, change to OPERATIONAL.\n", slave);
									ec_slave[slave].state = EC_STATE_OPERATIONAL;
									ec_writestate(slave);										
								}
								else if(ec_slave[slave].state > 0)
								{
									if (ec_reconfig_slave(slave))
									{
										ec_slave[slave].islost = FALSE;
										printf("MESSAGE : slave %d reconfigured\n",slave);									
									}
								} 
								else if(!ec_slave[slave].islost)
								{
									ec_slave[slave].islost = TRUE;
									printf("ERROR : slave %d lost\n",slave);									
								}
							}
							if (ec_slave[slave].islost)
							{
								if(!ec_slave[slave].state)
								{
									if (ec_recover_slave(slave))
									{
										ec_slave[slave].islost = FALSE;
										printf("MESSAGE : slave %d recovered\n",slave);									
									}
								}
								else
								{
									ec_slave[slave].islost = FALSE;
									printf("MESSAGE : slave %d found\n",slave);									
								}
							}
						}
						if(!ec_group[0].docheckstate)
							printf("OK : all slaves resumed OPERATIONAL.\n");
					}
					else
					{
						printf("Processdata cycle %4d, WKC %d , O:", i, wkc);

						//for(j = 0 ; j < oloop; j++)
						for(j = 0 ; j < ec_slave[3].Obytes; j++)
						{
							printf(" %2.2x", *(ec_slave[3].outputs + j));
						}

						printf(" I:");					
						//for(j = 0 ; j < iloop; j++)
						for(j = 0 ; j < ec_slave[3].Ibytes; j++)
						{
							printf(" %2.2x", *(ec_slave[3].inputs + j));
						}
/*
                        printf("->%d ", (short)((unsigned short)ec_slave[5].inputs[2] << 8 |
                                             ((unsigned short)ec_slave[5].inputs[3])));
                        printf(" %d ", (short)((unsigned short)ec_slave[5].inputs[6] << 8 |
                                             ((unsigned short)ec_slave[5].inputs[7])));
                        printf(" %d ", (short)((unsigned short)ec_slave[5].inputs[10] << 8 |
                                             ((unsigned short)ec_slave[5].inputs[11])));
                        printf(" %d", (short)((unsigned short)ec_slave[5].inputs[14] << 8 |
                                             ((unsigned short)ec_slave[5].inputs[15])));

    */


/*
                        *(ec_slave[3].outputs + 2) = 0xFF;
                        *(ec_slave[3].outputs + 3) = 0x7F;
                        *(ec_slave[3].outputs + 0) = 0x00;
                        *(ec_slave[3].outputs + 1) = 0x00;
                        *(ec_slave[3].outputs + 4) = 0x00;
                        *(ec_slave[3].outputs + 5) = 0x00;
                        *(ec_slave[3].outputs + 6) = 0xFF;
                        *(ec_slave[3].outputs + 7) = 0x7F;
*/
                        // Test encoder read
                        //memset(&usdo, 0, 128);
                        //SDOsize = 6;
	                    //ec_SDOread(0x02, 0x6000, 0x11, FALSE, &SDOsize, &usdo, EC_TIMEOUTRXM);
	        			//i32 = (int32*) &usdo[0];
			        	//printf(" Enc1 value: 0x%8.8x %d", *i32, *i32); 
                        //int wc = ec_TxPDO(0x02, 0x1A00, &SDOsize, &usdo, EC_TIMEOUTRXM);
                        //printf(" S(%d-wc:%d)", SDOsize, wc); 
                        //for (counter = 0; counter < 6; counter++)
                        //{
                        //    printf (" %d", usdo[counter]);
                        //}
                        // Test encoder read end
                           
                        // Test encoder write
                        //memset(&usdo, 0, 128);
                        //usdo[0] = 0xFF;
                        /*uword = 0xFFFF;
	                    ec_SDOwrite(0x03, 0x7000, 0x01, FALSE, sizeof(uword), &uword, EC_TIMEOUTRXM);
	                    ec_SDOwrite(0x03, 0x7010, 0x01, FALSE, sizeof(uword), &uword, EC_TIMEOUTRXM);
	                    ec_SDOwrite(0x03, 0x7020, 0x01, FALSE, sizeof(uword), &uword, EC_TIMEOUTRXM);
	                    ec_SDOwrite(0x03, 0x7030, 0x01, FALSE, sizeof(uword), &uword, EC_TIMEOUTRXM);*/
                        // Test encoder write end

                        printf("\r");
						needlf = TRUE;
					}
					usleep(10000);
					
				}
			}
			else
			{
				printf("Not all slaves reached operational state.\n");
				ec_readstate();
				for(i = 1; i<=ec_slavecount ; i++)
				{
					if(ec_slave[i].state != EC_STATE_OPERATIONAL)
					{
						printf("Slave %d State=0x%2.2x StatusCode=0x%4.4x : %s\n",
							i, ec_slave[i].state, ec_slave[i].ALstatuscode, ec_ALstatuscode2string(ec_slave[i].ALstatuscode));
					}
				}
			}			
			printf("\nRequest safe operational state for all slaves\n");
			ec_slave[0].state = EC_STATE_INIT;
			/* request SAFE_OP state for all slaves */
			ec_writestate(0);
		}
		else
		{
			printf("No slaves found!\n");
		}
		printf("End simple test, close socket\n");
		/* stop SOEM, close socket */
		ec_close();
	}
	else
	{
		printf("No socket connection on %s\nExcecute as root\n",ifname);
	}	
}	

int main(int argc, char *argv[])
{
	printf("SOEM (Simple Open EtherCAT Master)\nSimple test\n");

	if (argc > 1)
	{		
		/* start cyclic part */
		simpletest(argv[1]);
	}
	else
	{
		printf("Usage: simple_test ifname1\nifname = eth0 for example\n");
	}	
	
	printf("End program\n");
	return (0);
}
