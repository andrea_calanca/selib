cmake_minimum_required(VERSION 2.8)

add_executable( prova prova.cpp puma260-component.cpp puma260-component.hpp )

target_link_libraries( prova ../../SOEM1.2.5/src/ethercatconfig.o ../../SOEM1.2.5/src/ethercatcoe.o ../../SOEM1.2.5/src/ethercatfoe.o ../../SOEM1.2.5/src/ethercatmain.o ../../SOEM1.2.5/src/ethercatbase.o ../../SOEM1.2.5/src/ethercatprint.o ../../SOEM1.2.5/src/ethercatsoe.o ../../SOEM1.2.5/src/nicdrv.o)

include_directories( ../../SOEM1.2.5/src)

add_definitions( "-O2 -lpthread -lm -lrt -lc -lpq -Wall -I../../SOEM1.2.5/src/"  )
