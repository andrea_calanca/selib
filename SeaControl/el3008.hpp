#ifndef EL3008_HPP
#define EL3008_HPP

#include "elXXXX.hpp"

// SOEM includes
extern "C" {
#include "ethercattype.h"
#include "nicdrv.h"
#include "ethercatbase.h"
#include "ethercatmain.h"
#include "ethercatcoe.h"
#include "ethercatfoe.h"
#include "ethercatconfig.h"
#include "ethercatprint.h"
}

/****************************************************************************
 * Copyright (C) 2015 Lorenzo Bertelli, Andrea Calanca
 * @author Lorenzo Bertelli, Andrea Calanca
 * @date june 2015
 ****************************************************************************/


using namespace std;

class EL3008 : public ELXXXX
{
    public:
        EL3008(int slaveID, ec_slavet * ec_slave);

        short getRaw(int channel);
        double getAnalog(int channel);


    private:
        double scale(short value);

};

#endif
