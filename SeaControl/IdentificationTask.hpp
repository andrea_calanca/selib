#ifndef IDENTIFICATIONTASK_H
#define IDENTIFICATIONTASK_H

#include <string>
#include"LoopTask.hpp"
#include"PositionTask.hpp"
#include"CurrentTask.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

/**
This class implements a finite state machine of series tasks that can be used to serialize identification experiments
*/
class IdentificationTask: public LoopTask
{
public:
    IdentificationTask(double* amps, double*  freqs, int n, ISEAHardware* hw);
    int _loop();

protected:
	int idx;
    int size;
    double* amp;
    double* freq;

private:
    SinPositionTask* pt;
};

/**
This class implements a finite state machine of series tasks to for identifing the friction curve in a torque/velocity plot
*/
class FrictionIdentificationTask: public IdentificationTask
{
public:
	FrictionIdentificationTask(double* amps, int n, ISEAHardware* hw): IdentificationTask(amps,amps,n,hw) {}
	int _loop();

private:
	CurrentTask* ct;
	VelocityTask* vt;

};

/**
This class implements a finite state machine of series tasks to set different sinusoidal motor currents
*/
class openLoopIdentificationTask: public IdentificationTask
{
public:
	openLoopIdentificationTask(double* amps, double*  freqs, int n, ISEAHardware* hw): IdentificationTask(amps,freqs,n,hw) {}
	int _loop();

private:
	SinCurrentTask* t;

};




#endif // IDENTIFICATIONTASK_H
