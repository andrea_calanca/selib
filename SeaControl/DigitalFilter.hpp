#ifndef DIGITALFILTER_H
#define DIGITALFILTER_H
#define MAX_ORDER 8

#include <iostream>
#include <math.h>

#include "defines.h"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

using namespace std;
/**
This class implements a digital filter of any order. Vector symbol are the same of matlab, filter function
vector b -> Numerator coefficients of rational transfer function
vector a -> Denominator coefficients of rational transfer function
The left value of arrays a and b are the z^n coefficients es.:
[a2 a1 a0] -> a2*z^2 + a1*z + a0
Note: the method process(double input) is supposed to be called every period T, where T is the sampling time.
*/
class DigitalFilter
{
	public:
		DigitalFilter(int order, double* a, double* b);

		double process(double input); ///< data process, to be called without jitter
		void clean(); ///< cleans up the filter internal state
		double getValue(){ return y[k];} ///< gets the last processed data

		static DigitalFilter* getLowPassFilterHz(double f); ///< builds a one pole low-pass filter with cut-off frequency f [Hz]
		static DigitalFilter* getLowPassDifferentiatorHz(double f); ///< builds an approximate differentiator filter with cut-off frequency f [Hz]
		static DigitalFilter* getResonatorHz(double f,double r); ///< builds a resonator filter at frequency f [Hz]


		static DigitalFilter* getDelay0();
		static DigitalFilter* getDelay1();
		static DigitalFilter* getDelay2();
		static DigitalFilter* getDelay3();
		static DigitalFilter* getDelay4();
		static DigitalFilter* getDelay5();
		static DigitalFilter* getDelay10();

		static DigitalFilter* getMeanFilter();

		static DigitalFilter* getDifferentiator(); ///< builds a finite difference filter

//		static DigitalFilter* get20HzDerivator(); ///< return a differentiator with a 20Hz pole - w.r.t. differentiating and then filtering you can spare a dt
//		static DigitalFilter* get50HzDerivator(); ///< return a differentiator with a 50Hz pole - w.r.t. differentiating and then filtering you can spare a dt

//		static DigitalFilter* get005HzFilter(); ///< supposes T = 1 / 3kHz
//		static DigitalFilter* get01HzFilter(); ///< supposes T = 1 / 3kHz
//		static DigitalFilter* get05HzFilter(); ///< supposes T = 1 / 3kHz
//		static DigitalFilter* get1HzFilter(); ///< supposes T = 1 / 3kHz
//		static DigitalFilter* get5HzFilter(); ///< supposes T = 1 / 3kHz
//		static DigitalFilter* get20HzFilter(); ///< supposes T = 1 / 3kHz
//		static DigitalFilter* get30HzFilter(); ///< supposes T = 1 / 3kHz
//		static DigitalFilter* get50HzFilter(); ///< supposes T = 1 / 3kHz
//		static DigitalFilter* get10HzFilter(); ///< supposes T = 1 / 3kHz
//		static DigitalFilter* get100HzFilter(); ///< supposes T = 1 / 3kHz
//		static DigitalFilter* get1000HzFilter(); ///< supposes T = 1 / 3kHz

		static DigitalFilter* get10HzButtFilter(); ///< supposes T = 1 / 3kHz
		static DigitalFilter* get20HzButtFilter(); ///< supposes T = 1 / 3kHz
		static DigitalFilter* get100HzButtFilter(); ///< supposes T = 1 / 3kHz
		static DigitalFilter* get1000HzButtFilter(); ///< supposes T = 1 / 3kHz


     private:

        double a[MAX_ORDER];
        double b[MAX_ORDER];
        double u[MAX_ORDER];
        double y[MAX_ORDER];
        int k;
        int order;

};


#endif // DIGITALFILTER_H
