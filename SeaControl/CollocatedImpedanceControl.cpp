#include "ImpedanceControl.hpp"
#include "defines.h"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

ImpedanceControlBase::ImpedanceControlBase(ISEAHardware* hw) : SEAControl(hw)
{
	this->filename = "impedance";
}

void ImpedanceControlBase::logOpen()
{
	filenameStream << filename << floor(drand()*100) <<"---k_des_" << k_des << "-d_des_" << d_des << ".csv";
	string s(filenameStream.str());
	logfile.open(s.c_str());
}

void ImpedanceControlBase::Log()
{
	logfile << ref << " ";
	logfile << dref << " ";
	logfile << ddref << " ";
	logfile << tau << " ";
	logfile << dtau << " ";
	logfile << theta_m << " ";
	logfile << dtheta_m << " ";
	logfile << theta_e << " ";
	logfile << dtheta_e << " ";
	logfile << k_des << " ";
	logfile << d_des << " ";
    logfile << log1 << " ";
    logfile << log2 << " ";
    logfile << log3 << " ";
    logfile << this->time << " ";
    logfile << endl;
}


double SimpleImpedanceControl::__process(double dt)
{
//	tau = JM*ddtheta_m - k_des*theta_m - d_des*dtheta_m + (k_des/K_SPRING)*torque + (d_des/K_SPRING)*dtorque;
	out = - k_des*theta_m - d_des*dtheta_m + (k_des/K_SPRING)*tau + (d_des/K_SPRING)*dtau;
	out += frictionTorqueCompensation(dtheta_m) * KT;
	// in pratica bisogna aggiungere attrito sul motore!!
	// si possono anche mettere i set points
	return out;
}



double PDImpedanceControl::__process(double dt)
{
	//impedance control with inner force loop
	pid.ref =  - k_des * theta_e - d_des  * dtheta_e;
	pid.dref =  - k_des * dtheta_e - d_des * ddtheta_e;
	pid.ddref =  0;
//	logEnabled = false;
	out = pid.process(tau,dtau,dt);
	return out;
}

double CollocatedImpedanceControl::__process(double dt)
{
	//collocated impedance control with inner force loop
	pid.ref =  - k_des * theta_m - d_des  * dtheta_m;
	pid.dref =  - k_des * dtheta_m - d_des * ddtheta_m;
	pid.ddref =  0;
//	logEnabled = false;
	out = pid.process(tau,dtau,dt);
	return out;
}

double CollocatedAdmittanceControl::__process(double dt)
{
	//collocated impedance control with inner force loop
	pid.ref  =  - (K_SPRING - k_des)/(K_SPRING * k_des) * tau - d_des/K_SPRING  * dtheta_m;
	pid.dref =  - (K_SPRING - k_des)/(K_SPRING * k_des) * dtau - d_des/K_SPRING  * ddtheta_m;
	pid.ddref =  0;
//	logEnabled = false;
	out = pid.process(theta_m,dtheta_m,dt);
	return out;
}

AdaptiveImpedanceControl::AdaptiveImpedanceControl(ISEAHardware* hw) : ImpedanceControlBase(hw)
{
	this->filename = "impedanceADinf";
	double f = 20;
	double lambda2 = powf(2*M_PI*f,2);
	double lambda1 = 0.05*sqrt(lambda2);
	c = new MRAdaptiveForceControl(lambda1,lambda2,hw);
//	c = new IndirectAdaptiveForceControl(lambda1,lambda2,hw);
	c->L = 2*M_PI*20.0;
	c->setAdaptationSpeed(10);
//	c = new MultiAdaptiveForceControl(lambda1,lambda2,hw);
	c->referenceModelEnabled = false;
	c->adaptationEnabled = true;
	c->logEnabled = true;
	c->filename = "adaptiveADinf2.csv";

//	c->ni = 0.5; //migliori risultati tra 0.5 e 1
//	c->setAdaptationSpeed(50);
};


double AdaptiveImpedanceControl::__process(double dt)
{
	//impedance control with inner force loop
	c->ref =  - k_des * theta_e - d_des  * dtheta_e;
	c->dref =  - k_des * dtheta_e - d_des * ddtheta_e;
	c->ddref =  - k_des * ddtheta_e;

	out = c->process(tau,dtau,dt);
	return out;
}

SMImpedanceControl::SMImpedanceControl(ISEAHardware* hw) : ImpedanceControlBase(hw)
{
	this->filename = "impedanceACC";
	double f = 20;
	double w = 2*M_PI*f;
	double lambda2 = powf(w,2);
	double lambda1 = 2.0*sqrt(lambda2);
	c = new IntegralSlidingModeForceControl(lambda1,lambda2,hw);
//	c = new SlidingModeForceControl(w,hw);
	c->ni = 0;
	c->phi = 5;
	c->logEnabled = false;
}


double SMImpedanceControl::__process(double dt)
{
	//impedance control with inner force loop
	c->ref =  - k_des * theta_e - d_des  * dtheta_e;
	c->dref =  - k_des * dtheta_e - d_des * ddtheta_e;
	c->ddref =  - k_des * ddtheta_e;
//	c->ni = 0;
	out = c->process(tau,dtau,dt);
	return out;
}


ValleryImpedanceControl::ValleryImpedanceControl(ISEAHardware* hw) : ImpedanceControlBase(hw)
{
	this->filename = "impedanceVallery";
	c = new PassiveValleryForceControl(hw);
	c->logEnabled = false;
	logEnabled = false;
}


double ValleryImpedanceControl::__process(double dt)
{
	//impedance control with inner force loop
	c->ref =  - k_des * theta_e - d_des  * dtheta_e;
	c->dref =  - k_des * dtheta_e - d_des * ddtheta_e;
	c->ddref =  - k_des * ddtheta_e;
	cout << ref;
	out = c->process(tau,dtau,dt);
	return out;
}
